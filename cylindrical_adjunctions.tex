\begin{chapter}{Cylindrical adjunctions} \label{CylindricalAdjunctionsChapter} 

We introduce a notion of adjunction between a cylinder and a co-cylinder, which is discussed for example in \S{3} of \cite{KampsPorterAbstractHomotopyAndSimpleHomotopyTheory}. In \ref{StructuresUponAnIntervalChapter} we shall explain that an interval in a category gives rise to a cylinder and co-cylinder which are adjoint. 

In \ref{HomotopyAndRelativeHomotopyChapter} we shall define a notion of homotopy with respect to a cylinder or a co-cylinder. Given both a cylinder and a co-cylinder, it will be vital for us to know that the corresponding notions of homotopy equivalence coincide. If the cylinder and co-cylinder are adjoint, we shall see that this is the case.

Furthermore, we shall in \ref{CofibrationsAndFibrationsChapter} define cofibrations with respect to a cylinder, and, dually, define fibrations with respect to a co-cylinder. If we have both a cylinder $\cylinder$ and a co-cylinder $\cocylinder$, with $\cylinder$ left adjoint to $\cocylinder$, we shall be able to characterise fibrations with respect to $\cocylinder$ via a homotopy lifting property with respect to $\cylinder$, and shall be able to characterise cofibrations with respect to $\cylinder$ via a homotopy lifting property with respect to $\cocylinder$. 

We refer the reader to Recollection \ref{AdjunctionRecollection} for the notion of an adjunction between 1-arrows of $\cl{C}$.

\begin{assum} Let $\cl{C}$ be a 2-category with a final object, and let $\cl{A}$ be an object of $\cl{C}$. \end{assum}

\begin{defn} \label{CylindricalAdjunctionDefinition}  Let $\cylinder = \big( \cyl, i_{0}, i_{1} \big)$ be a cylinder in $\cl{A}$, and let $\cocylinder = \big( \cocyl, e_{0}, e_{1} \big)$ be a co-cylinder in $\cl{A}$. Then $\cylinder$ is {\em left adjoint} to $\cocylinder$ if the following conditions are satisfied. 

\begin{itemize}[itemsep=1em,topsep=1em] 

\item[(i)]  $\cyl$ is left adjoint to $\cocyl$. 

\item[(ii)] Suppose that (i) holds. Let \ar{{\mathsf{Hom}_{\cl{A}}\big( \cyl(-),- \big)},{\mathsf{Hom}_{\cl{A}}\big(-,\cocyl(-)\big)},\mathsf{adj}} denote the corresponding natural isomorphism, adopting the shorthand of Recollection \ref{AdjunctionRecollection}. We require that for every arrow \ar{\cyl(a_{0}),a_{1},h} of $\cl{A}$, the following diagrams in $\cl{A}$ commute. \twosq{a_{0},\cyl(a_{0}),\cocyl(a_{1}),a_{1},i_{0}(a_{0}),h,\mathsf{adj}(h),e_{0}(a_{1}),a_{0},\cyl(a_{0}),\cocyl(a_{1}),a_{1},i_{1}(a_{0}),h,\mathsf{adj}(h),e_{1}(a_{1})} 

\end{itemize} 
\end{defn}

\begin{defn} \label{CylindricalAdjunctionCompatibleWithContractionAndExpansionDefinition} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, and let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$. 

Let \ar{{\mathsf{Hom}_{\cl{A}}\big( \cyl(-),- \big)},{\mathsf{Hom}_{\cl{A}}\big(-,\cocyl(-)\big)},\mathsf{adj}} denote the corresponding natural isomorphism, adopting the shorthand of Recollection \ref{AdjunctionRecollection}. The adjunction between $\cyl$ and $\cocyl$ is {\em compatible with $p$ and $c$} if, for every arrow \ar{a_{0},a_{1},f} of $\cl{A}$, the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{1},\cocyl(a_{1}),f,c(a_{1}),\mathsf{adj}\big(f \circ p(a_{0}) \big)} \end{defn}

\begin{rmk} Given a cylinder $\cylinder$ in $\cl{A}$, and a 1-arrow \ar[4]{\cl{A},\cl{A},\cocyl} of $\cl{C}$ which is left adjoint to $\cyl$, one can always equip $\cocyl$ with the structure of a co-cylinder $\cocylinder$ in $\cl{A}$ via the adjunction. 

Moreover, one can transfer structures upon $\cylinder$ across the adjunction to structures upon $\cocylinder$. This is explained for example in \S{1.8} of the paper \cite{GrandisMacDonaldHomotopyStructuresForAlgebrasOverAMonad} of Grandis and MacDonald. It goes back at least to \S{7} of the paper \cite{KampsKanBedingungenUndAbstrakteHomotopietheorie} of Kamps. \end{rmk}

\end{chapter}


