\begin{chapter}{Model category recollections} \label{ModelCategoryRecollectionsChapter} 

In \ref{ModelStructureChapter}, we shall bring together all the theory we have developed so far. In order to do so, we now present a few recollections on model categories. 

The notion of a model category was introduced by Quillen in \cite{QuillenHomotopicalAlgebra}. We recall two definitions, and prove that they are equivalent. Our arguments comprise part of the proof of Proposition 2 of \S{5} of \cite{QuillenHomotopicalAlgebra}. Our definitions are equivalent to the definition of a closed model category given in \S{5} of \cite{QuillenHomotopicalAlgebra}.

\begin{defn} \label{ModelStructureDefinition} Let $\cl{A}$ be a category with finite limits and colimits.  A {\em model structure} upon $\cl{A}$ consists of three sets $W$, $F$, and $C$ of arrows of $\cl{A}$, such that the following conditions are satisfied. 

\begin{itemize}[topsep=1em,itemsep=1em]

\item[(i)] If any two of the arrows in a commutative diagram \tri{a_{0},a_{1},a_{2},g_{0},g_{1},g_{2}} in $\cl{A}$ belong to $W$, so does the third. 

\item[(ii)] An arrow \ar{a_{2},a_{3},f} of $\cl{A}$ belongs to $F$ if and only if, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $j$ belongs to both $W$ and $C$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(iii)] An arrow \ar{a_{2},a_{3},f} of $\cl{A}$ belongs to both $F$ and $W$ if and only if, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $j$ belongs to $C$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(iv)] An arrow \ar{a_{0},a_{1},j} of $\cl{A}$ belongs to $C$ if and only if, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $f$ belongs to both $F$ and $W$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(v)] An arrow \ar{a_{0},a_{1},j} belongs to both $C$ and $W$ if and only if, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $f$ belongs to $F$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(vi)] For every arrow \ar{a_{0},a_{1},f} of $\cl{A}$, there is an arrow \ar{a_{0},a_{2},j} of $\cl{A}$ which belongs to $C$, and an arrow \ar{a_{2},a_{1},g} of $\cl{A}$ which belongs to $W$ and $F$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{2},a_{1},j,g,f}

\item[(vii)] For every arrow \ar{a_{0},a_{1},f} of $\cl{A}$, there is an arrow \ar{a_{0},a_{2},j} of $\cl{A}$ which belongs to $W$ and $C$, and an arrow \ar{a_{2},a_{1},g} of $\cl{A}$ which belongs to $F$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{2},a_{1},j,g,f}

\end{itemize}

\end{defn}

\begin{defn} Let $\cl{A}$ be a category with finite limits and colimits. Let $W$, $F$, and $C$ be sets of arrows of $\cl{A}$ which equip $\cl{A}$ with a model structure. We refer to an arrow of $\cl{A}$ which belongs to $W$ as a {\em weak equivalence}, to an arrow of $\cl{A}$ which belongs to $F$ as a {\em fibration}, and to an arrow of $\cl{A}$ which belongs to $C$ as a {\em cofibration}. We refer to an arrow of $\cl{A}$ which belongs to both $W$ and $C$ as a {\em trivial cofibration}, and to an arrow of $\cl{A}$ which belongs to both $W$ and $F$ as a {\em trivial fibration}. \end{defn}

\begin{defn} A {\em model category} is a category $\cl{A}$ which has finite limits and colimits, together with a model structure upon $\cl{A}$. \end{defn}

\begin{prpn} \label{EquivalentDefinitionsOfModelStructureProposition} Let $\cl{A}$ be a category with finite limits and colimits. Let $W$, $F$, $C$ be sets of arrows of $\cl{A}$. Then $(W,F,C)$ equips $\cl{A}$ with a model structure if and only if the following conditions are satisfied. 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] If any two of the arrows in a commutative diagram \tri{a_{0},a_{1},a_{2},g_{0},g_{1},g_{2}} in $\cl{A}$ belong to $W$, so does the third. 

\item[(ii)] Suppose that we have commutative diagrams \twosq{a_{2},a_{0},a_{3},a_{1},g_{0},j,j',g_{1},a_{0},a_{2},a_{1},a_{3},r_{0},j',j,r_{1}} in $\cl{A}$, such that $r_{0}$ is a retraction of $g_{0}$, and such that $r_{1}$ is a retraction of $g_{1}$. If $j$ belongs to $C$, then $j'$ belongs to $C$. If $j$ belongs to both $C$ and $W$, then $j'$ belongs to both $C$ and $W$.

\item[(iii)] Suppose that we have commutative diagrams \twosq{a_{2},a_{0},a_{3},a_{1},g_{0},f,f',g_{1},a_{0},a_{2},a_{1},a_{3},r_{0},f',f,r_{1}} in $\cl{A}$ such that $r_{0}$ is a retraction of $g_{0}$, and such that $r_{1}$ is a retraction of $g_{1}$. If $f$ belongs to $F$ then $f'$ belongs to $F$. If $f$ belongs to both $F$ and $W$ then $f'$ belongs to both $F$ and $W$.

\item[(iv)] For every diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, such that $j$ belongs to $W$ and $C$, and $f$ belongs to $F$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(v)] For every diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, such that $j$ belongs to $C$, and $f$ belongs to $W$ and $F$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes.

\item[(vi)] For every arrow \ar{a_{0},a_{1},f} of $\cl{A}$, there is an arrow \ar{a_{0},a_{2},j} of $\cl{A}$ which belongs to $C$, and an arrow \ar{a_{2},a_{1},g} of $\cl{A}$ which belongs to $W$ and $F$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{2},a_{1},j,g,f}

\item[(vii)] For every arrow \ar{a_{0},a_{1},f} of $\cl{A}$, there is an arrow \ar{a_{0},a_{2},j} of $\cl{A}$ which belongs to $W$ and $C$, and an arrow \ar{a_{2},a_{1},g} of $\cl{A}$ which belongs to $F$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{2},a_{1},j,g,f}

\end{itemize}

\end{prpn}

\begin{proof} We first prove that if the conditions of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} are satisfied, then $(W,F,C)$ equips $\cl{A}$ with a model structure. Let us demonstrate that condition (ii) of Definition \ref{ModelStructureDefinition} holds. 

Given that condition (iv) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} holds, it suffices to show that if \ar{a_{2},a_{3},f} is an arrow of $\cl{A}$ with the property that, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $j$ belongs to both $W$ and $C$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes, then $f$ belongs to $F$.

To this end, by condition (vii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition}, there is an arrow \ar{a_{2},a_{4},j'} of $\cl{A}$ which belongs to both $W$ and $C$, and an arrow \ar{a_{4},a_{3},f'} of $\cl{A}$ which belongs to $F$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{2},a_{4},a_{3},j',f',f} %
%
By assumption, there is an arrow \ar{a_{4},a_{2},l'} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{2},a_{2},a_{4},a_{3},id,f,j',f',l'} %
%
In other words, we have a pair of commutative diagrams in $\cl{A}$ as follows such that $l'$ is a retraction of $j'$. \twosq{a_{2},a_{4},a_{3},a_{3},j',f',f,id,a_{4},a_{2},a_{3},a_{3},l',f,f',id} %
%
Appealing to condition (iii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition}, we deduce that $f$ belongs to $F$. 

Next, let us demonstrate that condition (iii) of Definition \ref{ModelStructureDefinition} holds. Given that condition (v) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} holds, it suffices to show that if \ar{a_{2},a_{3},f} is now an arrow of $\cl{A}$ with the property that, for every commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ such that $j$ belongs to $C$, there is an arrow \ar{a_{2},a_{1},l} of $\cl{A}$ such that the diagram \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} in $\cl{A}$ commutes, then $f$ belongs to both $W$ and $F$.

To this end, by condition (vi) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition}, there is an arrow \ar{a_{2},a_{4},j'} of $\cl{A}$ which belongs to $C$, and an arrow \ar{a_{4},a_{3},f'} of $\cl{A}$ which belongs to both $F$ and $W$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{2},a_{4},a_{3},j',f',f} %
%
By assumption, there is an arrow \ar{a_{4},a_{2},l'} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{2},a_{2},a_{4},a_{3},id,f,j',f',l'} %
%
In other words, we have a pair of commutative diagrams in $\cl{A}$ as follows such that $l'$ is a retraction of $j'$. \twosq{a_{2},a_{4},a_{3},a_{3},j',f',f,id,a_{4},a_{2},a_{3},a_{3},l',f,f',id} %
%
Appealing to condition (iii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition}, we deduce that $f$ belongs to both $F$ and $W$. 

That conditions (iv) and (v) of Definition \ref{ModelStructureDefinition} hold, given that conditions (ii), (iv), (v), (vi), and (vii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} hold, follows formally, by duality, from the two arguments we have already given in this proof. 

Conversely, suppose that $(W,F,C)$ equips $\cl{A}$ with a model structure. We must demonstrate that conditions (ii) and (iii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} are satisfied. 

Suppose that we have commutative diagrams \twosq{a_{2},a_{0},a_{3},a_{1},g_{0}',f,f',g_{1}',a_{0},a_{2},a_{1},a_{3},r_{0},f',f,r_{1}} in $\cl{A}$, such that $r_{0}$ is a retraction of $g_{0}$, such that $r_{1}$ is a retraction of $g_{1}$, and such that $f$ belongs to $F$. %
%
Suppose that we have a commutative diagram in $\cl{A}$ as follows, in which $j$ belongs to both $C$ and $W$. \sq{a_{0}',a_{2},a_{1}',a_{3}',g_{0},f',j,g_{1}} %
%
Then the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{a_{0}',a_{0},a_{1}',a_{1},g_{0} \circ g_{0}',f,j,g_{1} \circ g_{1}'} %
%
Since $f$ belongs to $F$, by condition (ii) of Definition \ref{ModelStructureDefinition} there is an arrow \ar{a_{1}',a_{0},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare[{4,3}]{a_{0}',a_{0},a_{1}',a_{1},g_{0} \circ g_{0}',f,j,g_{1} \circ g_{1}',l} %
%
Thus the following diagram in $\cl{A}$ commutes. \liftingsquare[{6,3}]{a_{0}',a_{2},a_{1}',a_{3},r_{0} \circ g_{0} \circ g_{0}',f,j,r_{1} \circ g_{1} \circ g_{1}',r_{0} \circ l} %
%
Since $r_{0}$ is a retraction of $g_{0}$, and since $r_{1}$ is a retraction of $g_{1}$, we thus have that the following diagram in $\cl{A}$ commutes. \liftingsquare[{6,3}]{a_{0}',a_{2},a_{1}',a_{3},g_{0}',f,j,g_{1}',r_{0} \circ l} 

An entirely similar argument, appealing to condition (iii) rather than condition (ii) of Definition \ref{ModelStructureDefinition}, proves that if $f$ belongs to both $F$ and $W$, then $f'$ belongs to both $F$ and $W$. This completes our proof that condition (ii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} is satisfied. 

That condition (iii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} is satisfied, given that conditions (iv) and (v) of Definition \ref{ModelStructureDefinition} hold, follows formally, by duality, from the proof we have just given that condition (ii) of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} holds. 
 
\end{proof} 

\end{chapter}
