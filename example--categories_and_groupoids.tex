\begin{chapter}{Example --- categories and groupoids} \label{ExampleChapter}

We define an interval in the category $\mathsf{Cat}$ of categories, equipped with its cartesian monoidal structure. It admits all the structures of \ref{StructuresUponAnIntervalChapter}, and has strictness of identities. By \ref{ModelStructureChapter}, we thus obtain two model structures upon $\mathsf{Cat}$. In a non-constructive setting, both model structures can be proven to coincide with folk model structure. 

In the same way, we obtain two model structures upon the category $\mathsf{Grpd}$ of groupoids. Again both may be demonstrated, by a non-constructive argument, to coincide with the folk model structure.

The folk model structure on $\mathsf{Cat}$ was constructed by Joyal and Tierney in \cite{JoyalTierneyStrongStacksAndClassifyingSpaces}. Independently, a construction was given by Rezk in \cite{RezkAModelCategoryForCategories}. 

The folk model structure on $\mathsf{Grpd}$ appeared in the literature earlier. It was first described by Anderson in \S{5} of \cite{AndersonFibrationsAndGeometricRealizations}, and is also discussed in \S{14.1} of the paper \cite{BousfieldHomotopySpectralSequencesAndObstructions} of Bousfield. A detailed construction is given in \S{6.1} of the article \cite{StricklandKnLocalDualityForFiniteGroupsAndGroupoids} of Strickland, built upon in \S{3} of the thesis \cite{HollanderAHomotopyTheoryForStacks} of Hollander. 

The folk model structure on groupoids can also be seen to arise as the restriction to groupoids of the model structure on $\mathsf{Cat}$ constructed by Thomason in \cite{ThomasonCatAsAClosedModelCategory}. This is observed, for example, in \S{1} of the paper \cite{CasacubertaGolasinskiTonksHomotopyLocalizationOfGroupoids} of Casacuberta, Golasi{\'n}ski, and Tonks.

In all these works, the non-constructive characterisation of equivalences of categories as functors which are fully faithful and essentially surjective is essential. From this point of view, the folk model structure on $\mathsf{Cat}$ or $\mathsf{Grpd}$ is akin to the Serre model structure on topological spaces, which was first constructed in \S{II.3} of \cite{QuillenHomotopicalAlgebra}. 

The conceptual approach we have taken is significantly different. Our two model structures are instead akin to the model structure on topological spaces, which was constructed by Str{\o}m in \cite{StromTheHomotopyCategoryIsAHomotopyCategory}. The fact that we may non-constructively identify the two model structures on categories or groupoids which we construct with the folk model structure might reasonably, we think, be viewed as something of a coincidence.

\begin{notn} Let $\mathsf{Cat}$ denote the category of categories, and let $\mathsf{Grpd}$ denote the category of groupoids. We denote by $1$ the final object of $\mathsf{Cat}$ and $\mathsf{Grpd}$, the category with a unique object $\bullet$ and a unique arrow. \end{notn}

\begin{rmk} We shall regard $\mathsf{Cat}$ and $\mathsf{Grpd}$ as equipped with their cartesian monoidal structures. These monoidal structures are closed, and thus Requirement \ref{SubdivisionRequirement} is satisfied. \end{rmk}

\begin{notn} Let $\cl{I}$ denote the free groupoid on the following directed graph. \ar{0,1} \end{notn}

\begin{notn} Let \ar{1,\cl{I},i_{0}} denote the unique functor which maps $\bullet$ to $0$. \end{notn}

\begin{notn} Let \ar{1,\cl{I},i_{1}} denote the unique functor which maps $\bullet$ to $1$. \end{notn}

\begin{notn} Let $\interval$ denote the interval $\big( \cl{I},i_{0},i_{1} \big)$ in $\mathsf{Cat}$ or $\mathsf{Grpd}$. \end{notn}

\begin{observation} The canonical functor \ar{\cl{I},1,p} equips $\interval$ with a contraction structure. \end{observation} 

\begin{notn} Let \ar{\cl{I},\cl{I},v} denote the unique functor which maps the arrow \ar{0,1} of $\cl{I}$ to the arrow \ar{1,0} of $\cl{I}$. \end{notn}

\begin{observation} The functor $v$ equips $\interval$ with an involution structure which is compatible with $p$. \end{observation}

\begin{notn} Let $\cl{S}$ denote the free groupoid on the following directed graph. \twoar{0,1,2} \end{notn}

\begin{notn} Let \ar{\cl{I},\cl{S},r_{0}} denote the unique functor which maps the arrow \ar{0,1} of $\cl{I}$ to the arrow \ar{1,2} of $\cl{S}$. \end{notn}

\begin{notn} Let \ar{\cl{I},\cl{S},r_{1}} denote the unique functor which maps the arrow \ar{0,1} of $\cl{I}$ to the arrow \ar{0,1} of $\cl{S}$. \end{notn}

\begin{notn} Let \ar{\cl{I},\cl{S},s} denote the unique functor which maps the arrow \ar{0,1} of $\cl{I}$ to the arrow \ar{0,2} of $\cl{S}$. \end{notn}

\begin{observation} We have that $\big( \cl{S}, r_{0}, r_{1}, s \big)$ equips $\interval$ with a subdivision structure, which is compatible with $p$.
\end{observation}

\begin{observation} With respect to the involution structure $v$ and the subdivision structure $\big( S, r_{0}, r_{1}, s \big)$, the interval $\interval$ has strictness of identities and strictness of left inverses. \end{observation}

\begin{observation} The groupoid $\cl{I}^{2}= \cl{I} \times \cl{I}$ is the unique groupoid with objects and arrows as follows, excluding the four identity arrows. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=5 em, row sep=4 em, text height=1.6ex, text depth=0.25ex]
{ 
|(0-0)| (0,0) \& |(1-0)| (1,0) \&[2em] |(2-0)| (0,0) \& |(3-0)| (1,0) \\ 
|(0-1)| (0,1) \& |(1-1)| (1,1) \& |(2-1)| (0,1) \& |(3-1)| (1,1) \\
};
	
\draw[->] (0-0) to node[auto] {} (1-0);
\draw[->] (1-0) to node[auto] {} (1-1);
\draw[->] (0-0) to node[auto,swap] {} (0-1);
\draw[->] (0-1) to node[auto,swap] {} (1-1);
\draw[->] (3-0) to node[auto,swap] {} (2-0);
\draw[->] (3-1) to node[auto,swap] {} (3-0);
\draw[->] (2-1) to node[auto] {} (2-0);
\draw[->] (3-1) to node[auto] {} (2-1);
\draw[->] (0-0) to node[auto,swap] {} (1-1);
\draw[->] (3-1) to node[auto] {} (2-0);

\end{tikzpicture} 

\end{diagram} 

\end{observation}

\begin{notn} Let \ar{\cl{I}^{2},\cl{I},\Gamma_{ul}} denote the unique functor with the following properties: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] the arrow \ar{{(0,0)},{(1,0)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,1} of $\cl{I}$. 

\item[(ii)] the arrow \ar{{(0,0)},{(0,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,1} of $\cl{I}$. 

\item[(iii)] the arrow \ar{{(1,0)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{1,1} of $\cl{I}$. 

\item[(iv)] the arrow \ar{{(0,1)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{1,1} of $\cl{I}$.

\end{itemize}

\end{notn} 

\begin{observation} The functor $\Gamma_{ul}$ equips $\interval$ with an upper left connection structure. \end{observation}

\begin{notn} Let \ar{\cl{I}^{2},\cl{I},\Gamma_{lr}} denote the unique functor with the following properties: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] the arrow \ar{{(0,0)},{(1,0)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,0} of $\cl{I}$. 

\item[(ii)] the arrow \ar{{(0,0)},{(0,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,0} of $\cl{I}$. 

\item[(iii)] the arrow \ar{{(1,0)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,1} of $\cl{I}$. 

\item[(iv)] the arrow \ar{{(0,1)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,1} of $\cl{I}$.

\end{itemize}

\end{notn} 

\begin{observation} The functor $\Gamma_{lr}$ equips $\interval$ with a lower right connection structure, which is compatible with $p$. \end{observation}

\begin{notn} Let \ar{\cl{I}^{2},\cl{I},\Gamma_{ur}} denote the unique functor with the following properties: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] the arrow \ar{{(0,0)},{(1,0)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,1} of $\cl{I}$. 

\item[(ii)] the arrow \ar{{(0,0)},{(0,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,0} of $\cl{I}$. 

\item[(iii)] the arrow \ar{{(1,0)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{1,0} of $\cl{I}$. 

\item[(iv)] the arrow \ar{{(0,1)},{(1,1)}} of $\cl{I}^{2}$ maps to the arrow \ar{0,0} of $\cl{I}$.

\end{itemize}

\end{notn} 

\begin{observation} The functor $\Gamma_{ur}$ equips $\interval$ with an upper right connection structure. We have that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big(\cl{S},r_{0},r_{1},s\big)$. \end{observation}

\begin{observation} A functor is a homotopy equivalence with respect to $\cylinderinterval$ if and only if it is an equivalence of categories. \end{observation}  

\begin{recollection} An {\em iso-fibration} is a functor \ar{\cl{A}_{0},\cl{A}_{1},F} with the property that, for every commutative diagram \sq{1,\cl{A}_{0},\cl{I},\cl{A}_{1},a,F,i_{0},g} in $\mathsf{Cat}$, there is a functor \ar{\cl{I},\cl{A}_{0},l} such that the following diagram in $\mathsf{Cat}$ commutes. \liftingsquare{1,\cl{A}_{0},\cl{I},\cl{A}_{1},a,F,i_{0},g,l} \end{recollection}

\begin{recollection} A {\em normally cloven iso-fibration} is a functor \ar{\cl{A}_{0},\cl{A}_{1},F} with the property that, to every commutative diagram \sq{1,\cl{A}_{0},\cl{I},\cl{A}_{1},a,F,i_{0},g} in $\mathsf{Cat}$, we can associate a functor \ar{\cl{I},\cl{A}_{0},l} such that the following hold.

\begin{itemize}[itemsep=1em,topsep=1em] 

\item[(i)] The diagram \liftingsquare{1,\cl{A}_{0},\cl{I},\cl{A}_{1},a,F,i_{0},g,l} in $\mathsf{Cat}$ commutes.

\item[(ii)] If the diagram \tri{\cl{I},1,\cl{A}_{1},p,F(a),g} in $\mathsf{Cat}$ commutes, then the diagram \tri{\cl{I},1,\cl{A}_{0},p,a,l} in $\mathsf{Cat}$ commutes.

\end{itemize}
\end{recollection}

\begin{observation} A functor \ar{\cl{A}_{0},\cl{A}_{1},F} is a fibration with respect to $\cylinderinterval$ if and only if it is an iso-fibration. This goes back to Proposition 2.1 of the paper \cite{BrownFibrationsOfGroupoids} of Brown. 

Moreover, $F$ is a normally cloven fibration with respect to $\cylinderinterval$ if and only if it is a normally cloven iso-fibration. \end{observation}

\begin{defn} An {\em iso-cofibration} is a functor \ar{\cl{A}_{0},\cl{A}_{1},j} such that $j$ is a cofibration with respect to $\cylinderinterval$. \end{defn} 

\begin{rmk} Non-constructively, it is possible to characterise an iso-cofibration as a functor which is injective on objects. \end{rmk}

\begin{defn} A {\em normally cloven iso-cofibration} is a functor \ar{\cl{A}_{0},\cl{A}_{1},j} such that $j$ is a normally cloven cofibration with respect to $\cylinderinterval$. \end{defn} 

\begin{thm} We obtain a model structure on $\mathsf{Cat}$ and $\mathsf{Grpd}$ by taking:

\begin{itemize}[itemsep=1em, topsep=1em]

\item[(i)] weak equivalences to be equivalences of categories,

\item[(ii)] fibrations to be iso-fibrations,

\item[(iii)] cofibrations to be normally cloven iso-cofibrations.

\end{itemize}
\end{thm}

\begin{proof} Follows immediately from Corollary \ref{NormallyClovenCofibrationsFibrationsIntervalModelStructureCorollary}. \end{proof}

\begin{thm} We obtain a model structure on $\mathsf{Cat}$ and $\mathsf{Grpd}$ by taking:

\begin{itemize}[itemsep=1em, topsep=1em]

\item[(i)] weak equivalences to be equivalences of categories,

\item[(ii)] fibrations to be normally cloven iso-fibrations,

\item[(iii)] cofibrations to be iso-cofibrations.

\end{itemize}
\end{thm}

\begin{proof} Follows immediately from Corollary \ref{CofibrationsNormallyClovenFibrationsIntervalModelStructureCorollary}. \end{proof}

\end{chapter}
