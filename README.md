# README #

## Abstract ##

We build a model structure from the simple point of departure of a structured interval in a monoidal category — more generally, a structured cylinder and a structured co-cylinder in a category.

## To compile ##

Run the following, in order. 

* pdflatex cylindrical_model_structures.tex
* bibtex cylindrical_model_structures.aux
* pdflatex cylindrical_model_structures.tex
* pdflatex cylindrical_model_structures.tex

## Questions or comments ##

Very welcome! Please contact me at richard (at) rwilliamson-mathematics.info.
