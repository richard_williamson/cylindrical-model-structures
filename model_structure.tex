\begin{chapter}{Model structure} \label{ModelStructureChapter}

Suppose that we have a cylinder $\cylinder$ and a co-cylinder $\cocylinder$ in a category $\cl{A}$, such that the following hold:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] both $\cylinder$ and $\cocylinder$ are equipped with all the structures we have considered in this work, and have strictness of identities;

\item[(ii)] $\cylinder$ is left adjoint to $\cocylinder$, and the adjunction between $\cyl$ and $\cocyl$ is compatible with their respective contraction structures.

\end{itemize} %
%
We bring all our theory together, to prove that we obtain a model structure upon $\cl{A}$ by taking:

\begin{itemize} [itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be homotopy equivalences with respect to $\cylinder$, or equivalently with respect to $\cocylinder$,

\item[(ii)] fibrations to be fibrations with respect to $\cocylinder$,

\item[(iii)] cofibrations to be normally cloven cofibrations with respect to $\cylinder$.

\end{itemize} %
%
Equally, we prove that we obtain a model structure upon $\cl{A}$ by taking:

\begin{itemize} [itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be homotopy equivalences with respect to $\cylinder$, or equivalently with respect to $\cocylinder$;

\item[(ii)] fibrations to be normally cloven fibrations with respect to $\cocylinder$;

\item[(iii)] cofibrations to be cofibrations with respect to $\cylinder$.

\end{itemize} %
%
An interval $\interval$ with respect to a monoidal structure upon $\cl{A}$ gives rise, as in \ref{StructuresUponAnIntervalChapter}, to a cylinder $\cylinderinterval$ and a co-cylinder $\cocylinderinterval$ in $\cl{A}$, under certain conditions. In this way, we also obtain two model structures upon $\cl{A}$ from an interval $\interval$ in a monoidal category, equipped with all the structures we have considered in this work, and satisfying strictness of identities. 

\begin{assum} Let $\cl{A}$ be a category with finite limits and colimits. \end{assum} 

\begin{rmk} We make this assumption to be consistent with the definition of a model category which was recalled in \ref{ModelCategoryRecollectionsChapter}. Our work in fact relies only upon the existence of mapping cylinders and mapping co-cylinders in $\cl{A}$. 

This is a significant difference. Mapping cylinders and mapping co-cylinders exist in the category of chain complexes in any additive category, for example, whereas arbitrary finite limits and colimits do not. \end{rmk}

\begin{thm} \label{CofibrationsNormallyClovenFibrationsModelStructureTheorem} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr}, \Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with: 

\begin{itemize} [itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $p$,

\item[(ii)] an involution structure $v$ compatible with $p$,

\item[(iii)] a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$,

\item[(iv)] an upper left connection structure $\Gamma_{ul}$, 

\item[(v)] a lower right connection structure $\Gamma_{lr}$ compatible with $p$,

\item[(vi)] an upper right connection structure $\Gamma_{ur}$.

\end{itemize} %
%
Suppose that: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( \subdiv, r_{0}, r_{1}, s \big)$, 

\item[(ii)] $\cyl$ preserves mapping cylinders with respect to $\cylinder$,

\item[(iii)] $\cylinder$ has strictness of identities.

\end{itemize} %
%
Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v', \subdiv',r_{0}', r_{1}', s', \Gamma_{ul}', \Gamma_{lr}' \big)$ be a co-cylinder in $\cl{A}$ equipped with:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $c$, 

\item[(ii)] an involution structure $v'$ compatible with $c$, 

\item[(iii)] a subdivision structure $\big(\subdiv', r_{0}', r_{1}', s' \big)$ compatible with $c$, 

\item[(iv)] an upper left connection structure $\Gamma_{ul}'$,

\item[(v)]  a lower right connection structure $\Gamma_{lr}'$ compatible with $c$. 

\end{itemize} %
% 
Suppose that:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$,

\item[(ii)] $\cocylinder$ has strictness of identities.

\end{itemize} %
%
Suppose moreover that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

We obtain a model structure upon $\cl{A}$ by taking: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be the homotopy equivalences with respect to $\cylinder$, or equivalently, by Proposition \ref{HomotopyEquivalenceCylinderIffHomotopyEquivalenceCoCylinderProposition}, to be the homotopy equivalences with respect to $\cocylinder$;

\item[(ii)] fibrations to be the normally cloven fibrations with respect to $\cocylinder$;

\item[(iii)] cofibrations to be the cofibrations with respect to $\cylinder$.

\end{itemize} 

\end{thm}

\begin{proof} That the conditions of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} hold has been established as follows:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] Proposition \ref{TwoOutOfThreeHomotopyEquivalencesProposition},

\item[(ii)] Proposition \ref{RetractionCofibrationIsCofibrationProposition} and Corollary \ref{RetractionTrivialCofibrationIsTrivialCofibrationCorollary},

\item[(iii)] Corollary \ref{RetractionNormallyClovenFibrationIsNormallyClovenFibrationCorollary} and Corollary \ref{RetractionTrivialNormallyClovenFibrationIsTrivialNormallyClovenFibrationCorollary},

\item[(iv)] Corollary \ref{NormallyClovenFibrationRLPTrivialCofibrationCorollary},

\item[(v)] Corollary \ref{TrivialNormallyClovenFibrationRLPCofibrationCorollary},

\item[(vi)] Corollary \ref{CofibrationFollowedByTrivialNormallyClovenFibrationCorollary},

\item[(vii)] Corollary \ref{TrivialCofibrationFollowedByNormallyClovenFibrationCorollary}. 

\end{itemize}
\end{proof}

\begin{thm} \label{NormallyClovenCofibrationsFibrationsModelStructureTheorem} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $p$,

\item[(ii)] an involution structure $v$ compatible with $p$,

\item[(iii)] a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$,

\item[(iv)] an upper left connection structure $\Gamma_{ul}$, 

\item[(v)] a lower right connection structure $\Gamma_{lr}$ compatible with $p$.
\end{itemize} %
%
Suppose that: 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\cyl$ preserves mapping cylinders with respect to $\cylinder$,

\item[(ii)] $\cylinder$ has strictness of identities.

\end{itemize} %
%
Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v', \subdiv',r_{0}', r_{1}', s', \Gamma_{ul}', \Gamma_{lr}', \Gamma_{ur}' \big)$ be a co-cylinder in $\cl{A}$ equipped with:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $c$, 

\item[(ii)] an involution structure $v'$ compatible with $c$, 

\item[(iii)] a subdivision structure $\big(\subdiv', r_{0}', r_{1}', s' \big)$ compatible with $c$, 

\item[(iv)] an upper left connection structure $\Gamma_{ul}'$,

\item[(v)] a lower right connection structure $\Gamma_{lr}'$ compatible with $c$,

\item[(vi)] an upper right connection structure $\Gamma_{ur}'$.

\end{itemize} %
% 
Suppose that:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\Gamma_{lr}'$ and $\Gamma_{ur}'$ are compatible with $\big( \subdiv', r_{0}', r_{1}', s' \big)$. 

\item[(ii)] $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$,

\item[(iii)] $\cocylinder$ has strictness of identities.

\end{itemize} %
%
Suppose moreover that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$.

We obtain a model structure upon $\cl{A}$ by taking:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be the homotopy equivalences with respect to $\cylinder$, or equivalently, by Proposition \ref{HomotopyEquivalenceCylinderIffHomotopyEquivalenceCoCylinderProposition}, the homotopy equivalences with respect to $\cocylinder$;

\item[(ii)] fibrations to be the fibrations with respect to $\cocylinder$;

\item[(iii)] cofibrations to be the normally cloven cofibrations with respect to $\cylinder$.

\end{itemize} 

\end{thm}

\begin{proof} That the conditions of Proposition \ref{EquivalentDefinitionsOfModelStructureProposition} hold has been established as follows:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] Proposition \ref{TwoOutOfThreeHomotopyEquivalencesProposition},

\item[(ii)] Proposition \ref{RetractionNormallyClovenCofibrationIsNormallyClovenCofibrationProposition} and Corollary \ref{RetractionTrivialNormallyClovenCofibrationIsTrivialNormallyClovenCofibrationCorollary},

\item[(iii)] Corollary \ref{RetractionFibrationIsFibrationCorollary} and Corollary \ref{RetractionTrivialFibrationIsTrivialFibrationCorollary},

\item[(iv)] Corollary \ref{FibrationRLPTrivialNormallyClovenCofibrationCorollary},

\item[(v)] Corollary \ref{TrivialFibrationRLPNormallyClovenCofibrationCorollary}, 

\item[(vi)] Corollary \ref{NormallyClovenCofibrationFollowedByTrivialFibrationCorollary}, 

\item[(vii)] Corollary \ref{TrivialNormallyClovenCofibrationFollowedByFibrationCorollary}. 

\end{itemize}
\end{proof}

\begin{assum} Let $\otimes$ be a monoidal structure upon $\cl{A}$. \end{assum}

\begin{cor} \label{CofibrationsNormallyClovenFibrationsIntervalModelStructureCorollary} Let $\interval = \big( I, i_{0}, i_{1}, p, v, S, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr}, \Gamma_{ur} \big)$ be an interval in $\cl{A}$ equipped with:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $p$,

\item[(ii)] an involution structure $v$ compatible with $p$,

\item[(iii)] a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$ compatible with $p$,

\item[(iv)] an upper left connection structure $\Gamma_{ul}$, 

\item[(v)] a lower right connection structure $\Gamma_{lr}$ compatible with $p$,

\item[(vi)] an upper right connection structure $\Gamma_{ur}$.

\end{itemize} %
%
Suppose that:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( S, r_{0}, r_{1}, s \big)$,

\item[(ii)] $I$ and $S$ are exponentiable with respect to $\otimes$,

\item[(iii)] Requirement \ref{SubdivisionRequirement} holds,

\item[(iv)] $\interval$ has strictness of identities.  
 
\end{itemize} %
%
We obtain a model structure upon $\cl{A}$ by taking:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be the homotopy equivalences with respect to $\cylinderinterval$, or equivalently, by Proposition \ref{HomotopyEquivalenceCylinderIffHomotopyEquivalenceCoCylinderProposition}, the homotopy equivalences with respect to $\cocylinderinterval$;

\item[(ii)] fibrations to be the normally cloven fibrations with respect to $\cocylinderinterval$;

\item[(iii)] cofibrations to be the cofibrations with respect to $\cylinderinterval$.

\end{itemize}

\end{cor}

\begin{proof} Follows immediately from Theorem \ref{CofibrationsNormallyClovenFibrationsModelStructureTheorem} by the observations of \ref{StructuresUponAnIntervalChapter}. \end{proof}

\begin{cor} \label{NormallyClovenCofibrationsFibrationsIntervalModelStructureCorollary} Let $\interval = \big( I, i_{0}, i_{1}, p, v, S, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr}, \Gamma_{ur} \big)$ be an interval in $\cl{A}$ equipped with:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] a contraction structure $p$,

\item[(ii)] an involution structure $v$ compatible with $p$,

\item[(iii)] a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$ compatible with $p$,

\item[(iv)] an upper left connection structure $\Gamma_{ul}$, 

\item[(v)] a lower right connection structure $\Gamma_{lr}$ compatible with $p$,

\item[(vi)] an upper right connection structure $\Gamma_{ur}$.

\end{itemize} %
%
Suppose that:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( S, r_{0}, r_{1}, s \big)$,

\item[(ii)] $I$ and $S$ are exponentiable with respect to $\otimes$,

\item[(iii)] Requirement \ref{SubdivisionRequirement} holds,

\item[(iv)] $\interval$ has strictness of identities.  
\end{itemize} %
%
We obtain a model structure upon $\cl{A}$ by taking:

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(i)] weak equivalences to be the homotopy equivalences with respect to $\cylinderinterval$, or equivalently, by Proposition \ref{HomotopyEquivalenceCylinderIffHomotopyEquivalenceCoCylinderProposition} the homotopy equivalences with respect to $\cocylinderinterval$;

\item[(ii)] fibrations to be the fibrations with respect to $\cocylinderinterval$;

\item[(iii)] cofibrations to be the normally cloven cofibrations with respect to $\cylinderinterval$.

\end{itemize}

\end{cor}

\begin{proof} Follows immediately from Theorem \ref{NormallyClovenCofibrationsFibrationsModelStructureTheorem} by the observations of \ref{StructuresUponAnIntervalChapter}. \end{proof}

\end{chapter}
