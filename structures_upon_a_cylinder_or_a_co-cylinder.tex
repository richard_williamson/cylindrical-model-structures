\begin{chapter}{Structures upon a cylinder or a co-cylinder} \label{StructuresUponACylinderOrACoCylinderChapter}

We introduce the notion of a cylinder or a co-cylinder in a formal category. We define the structures --- contraction, involution, subdivision, and three flavours of connection --- upon a cylinder and a co-cylinder which play a role in our work, and introduce axioms expressing their compatibility. We refer the reader to \ref{ExampleChapter} for an example of these structures in the category of categories. 

These structures and axioms have previously appeared in the literature. However, the precise definitions and terminology vary from author to author and paper to paper. Thus we collect in one place and from a single point of view all that we shall need. Most of our structures and axioms can be found in \S{4} of Chapter I and at the end of \S{3} of Chapter II of the book \cite{KampsPorterAbstractHomotopyAndSimpleHomotopyTheory} of Kamps and Porter, or in \S{2.1} and \S{2.3} of the paper \cite{GrandisCategoricallyAlgebraicFoundationsForHomotopicalAlgebra} of Grandis. Compatibility of right connections with subdivision appears implicitly, and in a slightly different context, in \S{6.4} of \cite{BrownHigginsSiveraNonabelianAlgebraicTopology}.
 
We also introduce strictness hypotheses which our structures upon a cylinder or a co-cylinder may satisfy. We shall come in \ref{HomotopyAndRelativeHomotopyChapter} to define the notion of a homotopy with respect to a cylinder or a co-cylinder. The first of our strictness hypotheses, which we shall refer to as strictness of left or right identities, ensures that the left or right composition of an identity homotopy with a homotopy $h$ is exactly $h$. 

Strictness of identities will also allow us to prove in \ref{FactorisationAxiomsChapter} that the mapping cylinder (respectively the mapping co-cylinder) of any arrow $f$ yields a factorisation of $f$ into a normally cloven cofibration followed by a trivial fibration (respectively into a trivial cofibration followed by a normally cloven fibration). It will moreover be crucial in establishing that the lifting axioms for a model category hold, in \ref{LiftingAxiomsChapter}.

The significance of strictness of identities with regard to lifting has to the author's knowledge not previously been observed. Its importance with regard to factorisation was independently identified by van den Berg and Garner in \cite{VanDenBergGarnerTopologicalAndSimplicialModelsOfIdentityTypes}. We particularly draw the reader's attention to Remark 4.3.3 in \cite{VanDenBergGarnerTopologicalAndSimplicialModelsOfIdentityTypes}. Our strictness of left (respectively right) identities condition corresponds to the left (respectively right) unitality condition of van den Berg and Garner. 

The second of our hypotheses, which we shall refer to as strictness of left inverses, ensures that the composition of a homotopy with its inverse is exactly an identity homotopy. It also allows us, given a lower right connection structure $\Gamma_{lr}$, to construct an upper right connection structure $\Gamma_{ur}$ such that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with subdivision. The compatibility of right connections with subdivision will be vital for us when in \ref{CoveringHomotopyExtensionPropertyChapter} we investigate the covering homotopy extension property.

\begin{assum} Let $\cl{C}$ be a 2-category, and let $\cl{A}$ be an object of $\cl{C}$. \end{assum}

\begin{defn} A {\em cylinder} in $\cl{A}$ is a 1-arrow \ar{\cl{A},\cl{A},\cyl} of $\cl{C}$, together with a pair of 2-arrows \pair{\id_{\cl{A}},\cyl,i_0,i_1} of $\cl{C}$. \end{defn}

\begin{defn} A {\em co-cylinder} in $\cl{A}$ is a 1-arrow \ar[4]{\cl{A},\cl{A},\cocyl} of $\cl{C}$, together with a pair of 2-arrows \pair{\cocyl,\id_{\cl{A}},e_{0},e_{1}} of $\cl{C}$. \end{defn}

\begin{rmk} Let $\cocylinder = \big(\cocyl,e_{0},e_{1} \big)$ be a co-cylinder in $\cl{A}$. Then $\big( \cocyl, e_{0}^{op}, e_{1}^{op} \big)$ defines a cylinder in $\cl{A}^{op}$, which we denote by $\cocylinder^{op}$. \end{rmk}

\begin{defn} Let $\cylinder = \big( \cyl, i_0, i_1 \big)$ be a cylinder in $\cl{A}$. A {\em contraction structure} with respect to $\cylinder$ is a 2-arrow \ar{\cyl,\id_{\cl{A}},p} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. \twotriangles{\id_{\cl{A}},\cyl,\id_{\cl{A}},i_{0},p,id,\id_{\cl{A}},\cyl,\id_{\cl{A}},i_{1},p,id} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_0, e_1 \big)$ be a co-cylinder in $\cl{A}$. A {\em contraction structure} with respect to $\cocylinder$ is a 2-arrow \ar{\id_{\cl{A}},\cocyl,c} of $\cl{C}$, such that $c^{op}$ equips the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ with a contraction structure. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl,i_{0},i_{1} \big)$ be a cylinder in $\cl{A}$. An {\em involution structure} with respect to $\cylinder$ is a 2-arrow \ar{\cyl,\cyl,v} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. \twotriangles{\id_{\cl{A}},\cyl,\cyl,i_{0},v,i_{1},\id_{\cl{A}},\cyl,\cyl,i_{1},v,i_{0}} \end{defn}
 
\begin{defn} Let $\cocylinder = \big( \cocyl,e_{0},e_{1} \big)$ be a co-cylinder in $\cl{A}$. An {\em involution structure} with respect to $\cocylinder$ is a 2-arrow \ar{\cocyl,\cocyl,v} of $\cl{C}$, such that $v^{op}$ defines an involution structure with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. An involution structure $v$ with respect to $\cylinder$ is {\em compatible with $p$} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri{\cyl,\cyl,\id_{\cl{A}},v,p,p} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. An involution structure $v$ with respect to $\cocylinder$ is {\em compatible with $c$} if the involution structure $v^{op}$ with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ is compatible with the contraction structure defined by $c^{op}$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl,i_{0},i_{1} \big)$ be a cylinder in $\cl{A}$. A {\em subdivision structure} with respect to $\cylinder$ is a 1-arrow \ar{\cl{A},\cl{A},\subdiv} of $\cl{C}$, together with a pair of 2-arrows \pair{\cyl,\subdiv,r_{0},r_{1}} of $\cl{C}$, such that the diagram \sq{\id_{\cl{A}},\cyl,\cyl,\subdiv,i_{0},r_{0},i_{1},r_{1}}in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ is co-cartesian, and a 2-arrow \ar{\cyl,\subdiv,s} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. \twosq{\id_{\cl{A}},\cyl,\cyl,\subdiv,i_{0},s,i_{0},r_{1},\id_{\cl{A}},\cyl,\cyl,\subdiv,i_{1},s,i_{1},r_{0}} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl,e_{0},e_{1} \big)$ be a co-cylinder in $\cl{A}$. A {\em subdivision structure} with respect to $\cocylinder$ is a 1-arrow \ar{\cl{A},\cl{A},\subdiv} of $\cl{C}$, together with a pair of 2-arrows \pair{\subdiv,\cocyl,r_{0},r_{1},s} of $\cl{C}$ and a 2-arrow \ar{\subdiv,\cocyl,s} of $\cl{C}$, such that $\big( \subdiv,r_{0}^{op},r_{1}^{op},s^{op} \big)$ defines a subdivision structure with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$. \end{defn}

\begin{defn} \label{SubdivisionCompatibleWithContractionDefinition} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, \subdiv, r_{0}, r_{1}, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big(\subdiv,r_{0},r_{1},s \big)$. Let \ar{\subdiv,\id_{\cl{A}},\overline{p}} denote the canonical 2-arrow of $\cl{C}$ such that the diagram \pushout{\id_{\cl{A}},\cyl,\cyl,\subdiv,\id_{\cl{A}},i_{0},r_{0},i_{1},r_{1},p,p,\overline{p}} in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. The subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ is {\em compatible with $p$} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri{\cyl,\subdiv,\id_{\cl{A}},s,\overline{p},p}  \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. A subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ with respect to $\cocylinder$ is {\em compatible with $c$} if the subdivision structure $\big( \subdiv, r_{0}^{op}, r_{1}^{op}, s^{op} \big)$ with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ is compatible with the contraction structure defined by $c^{op}$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl, i_0, i_1, \subdiv, r_0, r_1, s \big)$ be a cylinder in $\cl{A}$ equipped with a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then {\em $\cyl$ preserves subdivision} with respect to $\cylinder$ if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ is co-cartesian. \sq[{4,3}]{\cyl,\cyl^{2},\cyl^{2},\cyl \circ \subdiv,\cyl \cdot i_0, \cyl \cdot r_0, \cyl \cdot i_1, \cyl \cdot r_1} \end{defn} 

\begin{defn} Let $\cocylinder = \big( \cocyl, e_0, e_1, \subdiv, r_0, r_1, s \big)$ be a co-cylinder in $\cl{A}$ equipped with a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then {\em $\cocyl$ preserves subdivision} with respect to $\cocylinder$ if $\cocyl$ preserves subdivision with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the subdivision structure $\big( \subdiv, r_{0}^{op}, r_{1}^{op}, s^{op} \big)$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. An {\em upper left connection structure} with respect to $\cylinder$ is a 2-arrow \ar{\cyl^{2},\cyl,\Gamma_{ul}} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=4 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl         \& |(1-0)| \cyl^{2} \&[1em] |(2-0)| \cyl         \& |(3-0)| \cyl^{2} \\ 
                     \& |(1-1)| \cyl     \&                           \& |(3-1)| \cyl     \\
|(0-2)| \cyl         \& |(1-2)| \cyl^{2} \&      |(2-2)| \cyl         \& |(3-2)| \cyl^{2} \\
|(0-3)| \id_{\cl{A}} \& |(1-3)| \cyl     \&      |(2-3)| \id_{\cl{A}} \& |(3-3)| \cyl     \\      
};
	
\draw[->] (0-0) to node[auto] {$i_{0} \cdot \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$\Gamma_{ul}$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$id$} (1-1);
\draw[->] (2-0) to node[auto] {$\cyl \cdot i_{0}$} (3-0);
\draw[->] (3-0) to node[auto] {$\Gamma_{ul}$} (3-1);
\draw[->] (2-0) to node[auto,swap] {$id$} (3-1);
\draw[->] (0-2) to node[auto] {$i_{1} \cdot \cyl$} (1-2);
\draw[->] (1-2) to node[auto] {$\Gamma_{ul}$} (1-3);
\draw[->] (0-2) to node[auto,swap] {$p$} (0-3);
\draw[->] (0-3) to node[auto,swap] {$i_{1}$} (1-3);
\draw[->] (2-2) to node[auto] {$\cyl \cdot i_{1}$} (3-2);
\draw[->] (3-2) to node[auto] {$\Gamma_{ul}$} (3-3);
\draw[->] (2-2) to node[auto,swap] {$p$} (2-3);
\draw[->] (2-3) to node[auto,swap] {$i_{1}$} (3-3);

\end{tikzpicture} 

\end{diagram} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. An {\em upper left connection structure} with respect to $\cocylinder$ is a 2-arrow \ar{\cyl,\cyl^{2},\Gamma_{ul}} of $\cl{C}$, such that $(\Gamma_{ul})^{op}$ defines an upper left connection structure with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. A {\em lower right connection structure} with respect to $\cylinder$ is a 2-arrow \ar{\cyl^{2},\cyl,\Gamma_{lr}} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=4 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl         \& |(1-0)| \cyl^{2} \&[1em] |(2-0)| \cyl         \& |(3-0)| \cyl^{2} \\ 
                     \& |(1-1)| \cyl     \&                           \& |(3-1)| \cyl     \\
|(0-2)| \cyl         \& |(1-2)| \cyl^{2} \&      |(2-2)| \cyl         \& |(3-2)| \cyl^{2} \\
|(0-3)| \id_{\cl{A}} \& |(1-3)| \cyl     \&      |(2-3)| \id_{\cl{A}} \& |(3-3)| \cyl     \\      
};
	
\draw[->] (0-0) to node[auto] {$i_{1} \cdot \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$\Gamma_{lr}$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$id$} (1-1);
\draw[->] (2-0) to node[auto] {$\cyl \cdot i_{1}$} (3-0);
\draw[->] (3-0) to node[auto] {$\Gamma_{lr}$} (3-1);
\draw[->] (2-0) to node[auto,swap] {$id$} (3-1);
\draw[->] (0-2) to node[auto] {$i_{0} \cdot \cyl$} (1-2);
\draw[->] (1-2) to node[auto] {$\Gamma_{lr}$} (1-3);
\draw[->] (0-2) to node[auto,swap] {$p$} (0-3);
\draw[->] (0-3) to node[auto,swap] {$i_{0}$} (1-3);
\draw[->] (2-2) to node[auto] {$\cyl \cdot i_{0}$} (3-2);
\draw[->] (3-2) to node[auto] {$\Gamma_{lr}$} (3-3);
\draw[->] (2-2) to node[auto,swap] {$p$} (2-3);
\draw[->] (2-3) to node[auto,swap] {$i_{0}$} (3-3);

\end{tikzpicture} 

\end{diagram} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. A {\em lower right connection structure} with respect to $\cocylinder$ is a 2-arrow \ar{\cyl,\cyl^{2},\Gamma_{lr}} of $\cl{C}$, such that $(\Gamma_{lr})^{op}$ defines a lower right connection structure with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$. \end{defn}

\begin{defn} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. A lower right connection structure $\Gamma_{lr}$ with respect to $\cylinder$ is {\em compatible with $p$} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \sq{\cyl^{2},\cyl,\cyl,\id_{\cl{A}},\Gamma_{lr},p,p \cdot \cyl,p} \end{defn} 

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. A lower right connection structure $\Gamma_{lr}$ with respect to $\cocylinder$ is {\em compatible with $c$} if the lower right connection structure $(\Gamma_{lr})^{op}$ with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ is compatible with the contraction structure $c^{op}$. \end{defn} 

\begin{rmk} We shall not need to consider compatibility of an upper left connection structure with a contraction structure, or compatibility of an upper right connection structure, which we shall define next, with a contraction structure. \end{rmk}

\begin{defn} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$ and an involution structure $v$. An {\em upper right connection structure} with respect to $\cylinder$ is a 2-arrow \ar{\cyl^{2},\cyl,\Gamma_{ur}} of $\cl{C}$, such that the following diagrams in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commute. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=4 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl         \& |(1-0)| \cyl^{2} \&[1em] |(2-0)| \cyl         \& |(3-0)| \cyl^{2} \\ 
                     \& |(1-1)| \cyl     \&                           \& |(3-1)| \cyl     \\
|(0-2)| \cyl         \& |(1-2)| \cyl^{2} \&      |(2-2)| \cyl         \& |(3-2)| \cyl^{2} \\
|(0-3)| \id_{\cl{A}} \& |(1-3)| \cyl     \&      |(2-3)| \id_{\cl{A}} \& |(3-3)| \cyl     \\      
};
	
\draw[->] (0-0) to node[auto] {$i_{0} \cdot \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$\Gamma_{ur}$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$id$} (1-1);
\draw[->] (2-0) to node[auto] {$\cyl \cdot i_{1}$} (3-0);
\draw[->] (3-0) to node[auto] {$\Gamma_{ur}$} (3-1);
\draw[->] (2-0) to node[auto,swap] {$v$} (3-1);
\draw[->] (0-2) to node[auto] {$\cyl \cdot i_{0}$} (1-2);
\draw[->] (1-2) to node[auto] {$\Gamma_{ur}$} (1-3);
\draw[->] (0-2) to node[auto,swap] {$p$} (0-3);
\draw[->] (0-3) to node[auto,swap] {$i_{0}$} (1-3);
\draw[->] (2-2) to node[auto] {$i_{1} \cdot \cyl$} (3-2);
\draw[->] (3-2) to node[auto] {$\Gamma_{ur}$} (3-3);
\draw[->] (2-2) to node[auto,swap] {$p$} (2-3);
\draw[->] (2-3) to node[auto,swap] {$i_{0}$} (3-3);

\end{tikzpicture} 

\end{diagram} \end{defn}

\begin{defn} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$ and an involution structure $v$. An {\em upper right connection structure} with respect to $\cocylinder$ is a 2-arrow \ar{\cyl,\cyl^{2},\Gamma_{ur}}of $\cl{C}$, such that $(\Gamma_{ur})^{op}$ defines an upper right connection structure with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$ and the involution structure $v^{op}$. \end{defn}

\begin{rmk} \label{LowerLeftConnectionNotNecessaryRemark} Analogously, one can define a {\em lower left connection structure} with respect to a cylinder or a co-cylinder. Everything concerning upper and lower right connections below can equally be carried out for upper and lower left connections. \end{rmk}

\begin{defn} \label{RightConnectionsCylinderCompatibilityDefinition} Let $\cylinder = \big(\cyl,i_0,i_1,p,v,\subdiv,r_0,r_1,s,\Gamma_{lr},\Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s\big)$, an upper right connection structure $\Gamma_{ur}$, and a lower right connection structure $\Gamma_{lr}$. %
%
Let \ar{\subdiv \circ \cyl,\cyl,x} denote the canonical 2-arrow of $\cl{C}$ such that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \pushout[{4,3,-1}]{\cyl,\cyl^{2},\cyl^{2},\subdiv \circ \cyl,\cyl,i_0 \cdot \cyl,r_0 \cdot \cyl,i_1 \cdot \cyl,r_1 \cdot \cyl,\Gamma_{lr},\Gamma_{ur},x} %
%
Then $\Gamma_{lr}$ and $\Gamma_{ur}$ are {\em compatible with $\big( \subdiv, r_{0}, r_{1}, s \big)$} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri[{4,3}]{\cyl^{2},\subdiv \circ \cyl,\cyl,s \cdot \cyl,x,p \cdot \cyl} \end{defn}

\begin{defn} \label{RightConnectionsCoCylinderCompatibilityDefinition} Let $\cocylinder = \big(\cocyl,e_0, e_1, c, v, \subdiv, r_0, r_1, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, an upper right connection structure $\Gamma_{ur}$, and a lower right connection structure $\Gamma_{lr}$. 

Then $\Gamma_{lr}$ and $\Gamma_{ur}$ are {\em compatible with $\big( \subdiv, r_{0}, r_{1}, s \big)$} if the right connections $\Gamma_{lr}^{op}$ and $\Gamma_{ur}^{op}$ with respect to the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$ and the involution structure $v^{op}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}^{op}, r_{1}^{op}, s^{op} \big)$. \end{defn}

\begin{prpn} \label{LowerRightConnectionPlusInvolutionGivesUpperRightConnectionCylinderProposition} Let $\cylinder = \big( \cyl, i_0, i_1, p, v, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$ compatible with $p$, and a lower right connection structure $\Gamma_{lr}$. Then the 2-arrow \ar[7]{\cyl^{2},\cyl,\Gamma_{lr} \circ (v \cdot \cyl)} of $\cl{C}$ defines an upper right connection structure with respect to $\cylinder$. \end{prpn}

\begin{proof} Firstly, the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=6 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl \& |(1-0)| \cyl^{2} \\[2em] 
|(0-1)| \cyl \& |(1-1)| \cyl^{2} \\
};
	
\draw[->] (0-0) to node[auto] {$i_{0} \cdot \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$v \cdot \cyl$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$i_{1} \cdot \cyl$} (1-1);
\draw[->] (1-1) to node[auto] {$\Gamma_{lr}$} (0-1);
\draw[->] (0-0) to node[auto,swap] {$id$} (0-1);
\end{tikzpicture} 

\end{diagram} %
%
Secondly, the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes, appealing to the compatibility of $v$ with $p$. \trapeziums[{3,3,1,0}]{\cyl,\cyl^{2},\cyl,\cyl^{2},\id_{\cl{A}},\cyl,\cyl \cdot i_{0}, v \cdot \cyl, \Gamma_{lr}, p, i_{0}, v, \cyl \cdot i_{0}, p}  %
%
Thirdly, the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes.  

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=7 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl         \& |(1-0)| \cyl^{2} \\
|(0-1)| \id_{\cl{A}} \& |(1-1)| \cyl^{2} \\
                     \& |(1-2)| \cyl     \\
};
	
\draw[->] (0-0) to node[auto] {$i_{1} \cdot \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$v \cdot \cyl$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$i_{0} \cdot \cyl$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$p$} (0-1);
\draw[->] (0-1) to node[auto,swap] {$i_{0}$} (1-2);
\draw[->] (1-1) to node[auto] {$\Gamma_{lr}$} (1-2);

\end{tikzpicture} 

\end{diagram} %
%
Fourthly, the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \squareabovetriangle[{4,3,0}]{\cyl,\cyl^{2},\cyl,\cyl^{2},\cyl,\cyl \cdot i_{1},v \cdot \cyl, v, \cyl \cdot i_{1},\Gamma_{lr},id} \end{proof}

\begin{cor} \label{LowerRightConnectionPlusInvolutionGivesUpperRightConnectionCoCylinderCorollary} Let $\cocylinder = \big( \cocyl, e_0, e_1, c, v, \Gamma_{lr} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v$ compatible with $c$, and a lower right connection structure $\Gamma_{lr}$. Then the 2-arrow \ar[8]{\cocyl,\cocyl^{2},{(v \cdot \cocyl) \circ \Gamma_{lr}}} of $\cl{C}$ defines an upper right connection structure with respect to $\cocylinder$. \end{cor}

\begin{proof} Follows immediately from Proposition \ref{LowerRightConnectionPlusInvolutionGivesUpperRightConnectionCylinderProposition} by duality. \end{proof}

\begin{defn} \label{StrictnessLeftIdentitiesCylinderDefinition} Let $\cylinder = \big(\cyl, i_0, i_1, p, \subdiv, r_0, r_1, s \big)$ be a cylinder in $\cl{A}$, equipped with a contraction structure $p$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Let \ar{\subdiv,\cyl,q_{l}} denote the canonical 2-arrow of $\cl{C}$ such that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \pushout{\id_{\cl{A}},\cyl,\cyl,\subdiv,\cyl,i_{0},r_{0},i_{1},r_{1},id,i_{0} \circ p, q_{l}} %
%
Then $\cylinder$ has {\em strictness of left identities} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri{\cyl,\subdiv,\cyl,s,q_{l},id} \end{defn}

\begin{defn} \label{StrictnessLeftIdentitiesCoCylinderDefinition} Let $\cocylinder = \big( \cocyl, e_0, e_1, c, \subdiv, r_0, r_1, s \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then $\cocylinder$ has {\em strictness of left identities} if the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$ and the subdivision structure $\big( \subdiv^{op}, r_{0}^{op}, r_{1}^{op}, s^{op} \big)$ has strictness of left identities. \end{defn} 

\begin{defn} \label{StrictnessRightIdentitiesCylinderDefinition} Let $\cylinder = \big(\cyl, i_0, i_1, p, \subdiv, r_0, r_1, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Let \ar{\subdiv,\cyl,q_{r}} denote the canonical 2-arrow of $\cl{C}$ such that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \pushout{\id_{\cl{A}},\cyl,\cyl,\subdiv,\cyl,i_{0},r_{0},i_{1},r_{1},i_{1} \circ p,id,q_{r}} %
%
Then $\cylinder$ has {\em strictness of right identities} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri{\cyl,\subdiv,\cyl,s,q_{r},id} \end{defn}

\begin{defn} \label{StrictnessRightIdentitiesCoCylinderDefinition} Let $\cocylinder = \big( \cocyl, e_0, e_1, c, \subdiv, r_0, r_1, s \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then $\cocylinder$ has {\em strictness of right identities} if the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ equipped with the contraction structure $c^{op}$ and the involution structure $v^{op}$ has strictness of right identities. \end{defn} 

\begin{defn} \label{StrictnessIdentitiesCylinderDefinition} Let $\cylinder = \big(\cyl, i_0, i_1, p, \subdiv, r_0, r_1, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then $\cylinder$ has {\em strictness of identities} if it has both strictness of left identities and strictness of right identities. \end{defn}

\begin{defn} \label{StrictnessIdentitiesCoCylinderDefinition} Let $\cocylinder = \big( \cocyl, e_0, e_1, c, \subdiv, r_0, r_1, s \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then $\cocylinder$ has {\em strictness of identities} if it has both strictness of left identities and strictness of right identities. \end{defn}

\begin{defn} \label{StrictnessLeftInversesCylinderDefinition} Let $\cylinder = \big(\cyl, i_0, i_1, v, \subdiv, r_0, r_1, s \big)$ be a cylinder in $\cl{A}$ equipped with an involution structure $v$ and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Let \ar{\subdiv,\cyl,w} denote the canonical 2-arrow of $\cl{C}$ such that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \pushout{\id_{\cl{A}},\cyl,\cyl,\subdiv,\cyl,i_{0},r_{0},i_{1},r_{1},id,v,w} Then $\cylinder$ has {\em strictness of left inverses} if the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \sq{\cyl,\subdiv,\id_{\cl{A}},\cyl,s,w,p,i_1} \end{defn}

\begin{defn} \label{StrictnessLeftInversesCoCylinderDefinition} Let $\cocylinder = \big(\cocyl, e_0, e_1, v, \subdiv, r_0, r_1, s \big)$ be a co-cylinder in $\cl{A}$ equipped with an involution structure $v$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Then $\cocylinder$ has {\em strictness of left inverses} if the cylinder $\cocylinder^{op}$ in $\cl{A}^{op}$ has strictness of left inverses. \end{defn}

\begin{rmk} We shall not need to consider strictness of right inverses, for which there is an analogue of Proposition \ref{CriterionCompatibilityRightConnectionsWithSubdivisionCylinderProposition}. \end{rmk}

\begin{prpn} \label{CriterionCompatibilityRightConnectionsWithSubdivisionCylinderProposition} Let $\cylinder = \big(\cyl, i_0, i_1, p, v, \subdiv, r_0, r_1, s, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s\big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\cylinder$ has strictness of left inverses. Let $\Gamma_{ur}$ denote the upper right connection structure with respect to $\cylinder$ constructed in Proposition \ref{LowerRightConnectionPlusInvolutionGivesUpperRightConnectionCylinderProposition}. Then $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( \subdiv,r_{0},r_{1},s \big)$. \end{prpn}

\begin{proof} Let \ar{\subdiv,\cyl,w} denote the canonical 2-arrow of $\cl{C}$ of Definition \ref{StrictnessLeftInversesCylinderDefinition}. The following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri[{5,3}]{\cyl^{2},\subdiv \circ \cyl,\cyl^{2},r_{0} \cdot \cyl, w \cdot \cyl, id} Hence the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \tri[{5,3}]{\cyl^{2},\subdiv \circ \cyl, \cyl, r_{0} \cdot \cyl, \Gamma_{lr} \circ (w \cdot \cyl), \Gamma_{lr}} The following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ also commutes.  

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=5 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl^{2} \& |(1-0)| \subdiv \cyl \\ 
|(0-1)| \cyl \& |(1-1)| \cyl^{2} \\
};
	
\draw[->] (0-0) to node[auto] {$r_{1} \cyl$} (1-0);
\draw[->] (1-0) to node[auto] {$w \cdot \cyl$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$v \cdot \cyl$} (1-1);
\draw[->] (1-1) to node[auto] {$\Gamma_{lr}$} (0-1);
\draw[->] (0-0) to node[auto,swap] {$\Gamma_{ur}$} (0-1);
\end{tikzpicture} 

\end{diagram} %
%
Putting the last two observations together, we have that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \pushout[{4,3,-1}]{\cyl,\cyl^{2},\cyl^{2},\subdiv \circ \cyl, \cyl, i_{0} \cdot  \cyl, r_{0} \cdot \cyl, i_{1} \cdot \cyl, r_{1} \cdot \cyl, \Gamma_{lr}, \Gamma_{ur}, \Gamma_{lr} \circ (w \cdot \cyl)} %
%
By the universal property of $\subdiv \circ \cyl$, we deduce that $\Gamma_{lr} \circ (w \cdot \cyl) =x$, where \ar{S \circ \cyl,\cyl,x} is the canonical 2-arrow of $\cl{C}$ of Definition \ref{RightConnectionsCylinderCompatibilityDefinition}. 

The following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes. \squareabovetriangle[{5,3,0}]{\cyl^{2},\subdiv \circ \cyl,\cyl,\cyl^{2},\cyl,s \cdot \cyl,w \cdot \cyl,p \cdot \cyl, i_{1} \cdot \cyl, \Gamma_{lr},id} %
%
We conclude that the following diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A})$ commutes, as required. \tri[{4,3}]{\cyl^{2},\subdiv \circ \cyl,\cyl,s \cdot \cyl,x,p \cdot \cyl}  
\end{proof}

\begin{cor} \label{CriterionCompatibilityRightConnectionsWithSubdivisionCoCylinderCorollary} Let $\cocylinder = \big(\cocyl, e_0, e_1, c, v, \subdiv, r_0, r_1, s, \Gamma_{lr} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v$ compatible with $c$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s\big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\cocylinder$ has strictness of left inverses. Let $\Gamma_{ur}$ denote the upper right connection with respect to $\cocylinder$ of Corollary \ref{LowerRightConnectionPlusInvolutionGivesUpperRightConnectionCoCylinderCorollary}. Then $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( \subdiv, r_{0}, r_{1}, s \big)$. \end{cor}

\begin{proof} Follows immediately from Proposition \ref{CriterionCompatibilityRightConnectionsWithSubdivisionCylinderProposition} by duality. \end{proof}

\end{chapter}
