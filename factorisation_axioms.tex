\begin{chapter}{Factorisation axioms} \label{FactorisationAxiomsChapter}

Let $\cylinder$ be a cylinder in a formal category $\cl{A}$. In \ref{MappingCylindersAndMappingCoCylindersChapter}, we showed that if $\cylinder$ is equipped with certain structures, and has strictness of right identities, then the mapping cylinder with respect to $\cylinder$ of an arrow $f$ gives rise to a factorisation into a normally cloven cofibration followed by a strong deformation retraction.

We now prove that, if $\cylinder$ has strictness of left identities, then a strong deformation retraction with respect to $\cylinder$ is a trivial fibration with respect to $\cylinder$. Thus, if $\cylinder$ has strictness of both left and right identities, then the mapping cylinder of $f$ with respect to $\cylinder$ yields a factorisation of $f$ into a normally cloven cofibration followed by a trivial fibration. 

Dually, if a co-cylinder $\cocylinder$ in $\cl{A}$ is equipped with sufficient structures and has strictness of identities, the mapping co-cylinder of $f$ with respect to $\cocylinder$ yields a factorisation of $f$ into a trivial cofibration followed by a normally cloven fibration. 

Morever, building upon this, we construct a factorisation of $f$ into a cofibration followed by a trivial normally cloven fibration, and into a trivial normally cloven cofibration followed by a fibration.  

Neither the strictness of left identities hypothesis nor the strictness of right identities hypothesis holds with respect to the usual cylinder and co-cylinder in the category of topological spaces. Essentially, this is the observation that by glueing a path $g$ to a constant path, we obtain a path homotopic to $g$, but not $g$ itself. 

Whilst the mapping cylinder of a map between topological spaces gives a factorisation into a cofibration followed by a homotopy equivalence, this homotopy equivalence is not necessarily a fibration. An example is given by the mapping cylinder factorisation of the inclusion of a circle into a disc. Dually, whilst the mapping co-cylinder of a map between topological spaces gives a factorisation into a homotopy equivalence followed by a fibration, this homotopy equivalence is not necessarily a cofibration. An example is given by the mapping co-cylinder factorisation of any inclusion of spaces which is not closed. We refer the reader to \ref{IntroductionChapter} for a discussion of a way around this. 

We shall explore in \ref{ExampleChapter} a guiding example in which strictness of identities does hold, namely the homotopy theory of categories or groupoids.

\begin{assum} Let $\cl{C}$ be a 2-category with a final object. Suppose that pushouts and pullbacks of 2-arrows of $\cl{C}$ give rise to pushouts and pullbacks in formal categories, in the sense of Definition \ref{PushoutsPullbacks2ArrowsArePushoutsPullbacksInFormalCategoriesTerminology}. Let $\cl{A}$ be an object of $\cl{C}$. As before, we view $\cl{A}$ as a formal category, writing of objects and arrows of $\cl{A}$. \end{assum} 

\begin{prpn} \label{StrongDeformationRetractionIsFibrationProposition} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, \subdiv, r_{0}, r_{1}, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Suppose that $\cylinder$ has strictness of left identities. 

Let \ar{a_{1},a_{2},j} be an arrow of $\cl{A}$, and let \ar{a_{2},a_{1},f} be an arrow of $\cl{A}$ which is a retraction of $j$. Suppose that \ar{\cyl(a_{2}),a_{2},h} defines a homotopy over $a_{1}$ from $id(a_{2})$ to $jf$ with respect to $\cylinder$ and $(f,f)$. Then $f$ is a fibration with respect to $\cylinder$. \end{prpn}

\begin{proof} Suppose that we have a commutative diagram in $\cl{A}$ as follows. \sq{a_{0},a_{2},\cyl(a_{0}),a_{1},g,f,i_{0}(a_{0}),k} %
%
By definition of $h$, the following diagram in $\cl{A}$ commutes. \tri{a_{2},\cyl(a_{2}),a_{2},i_{1}(a_{2}),h,j \circ f} %
%
Appealing to the commutativity of the diagram \sq{a_{0},\cyl(a_{0}),a_{2},\cyl(a_{2}),i_{1}(a_{0}),\cyl(g),g,i_{1}(a_{2})} in $\cl{A}$, we deduce that the following diagram in $\cl{A}$ commutes. \tri{a_{0},\cyl(a_{0}),a_{2},i_{1}(a_{0}),h \circ \cyl(g),j \circ f \circ g} %
%
Moreover, the following diagram in $\cl{A}$ commutes. \tri{a_{0},\cyl(a_{0}),a_{2},i_{0}(a_{0}),j \circ k,j \circ f \circ g} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{a_{0},\cyl(a_{0}),\cyl(a_{0}),a_{2},i_{0}(a_{0}), j \circ k,i_{1}(a_{0}),h \circ \cyl(g)} %
%
Let \ar{\cyl(a_{0}),a_{2},l} denote the homotopy $\big(h \circ \cyl(g) \big) + (j \circ k)$ with respect to $\cylinder$. We claim that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},\cyl(a_{0}),a_{1},g,f,i_{0}(a_{0}),k,l} 

Firstly, the following diagram in $\cl{A}$ commutes. \squareabovetriangle{a_{0},\cyl(a_{0}),a_{2},\cyl(a_{2}),a_{2},i_{0}(a_{0}),\cyl(g),g,i_{0}(a_{2}),h,id} %
%
By definition of $l$, we also have that the following diagram in $\cl{A}$ commutes. \tri{a_{0},\cyl(a_{0}),a_{2},i_{0}(a_{0}),l,h \circ \cyl(g)} %
%
Hence the following diagram in $\cl{A}$ commutes, as required. \tri{a_{0},\cyl(a_{0}),a_{2},i_{0}(a_{0}),l,g} 

Secondly, let us prove that the diagram \tri{\cyl(a_{0}),a_{2},a_{1},l,f,k} in $\cl{A}$ commutes. Let \ar{\subdiv(a_{0}),a_{2},u} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{a_{0},\cyl(a_{0}),\cyl(a_{0}),\subdiv(a_{0}),a_{2},i_{0}(a_{0}),r_{0}(a_{0}),i_{1}(a_{0}),r_{1}(a_{0}),j \circ k, h \circ \cyl(g),u} %
%
By definition of $h$, the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{1}),a_{1},\cyl(a_{2}),a_{2},h,f,\cyl(f),p(a_{2})} %
%
Appealing to the commutativity of the diagram \tri{\cyl(a_{0}),\subdiv(a_{0}),a_{2},r_{1}(a_{0}),u,h \circ \cyl(g)} in $\cl{A}$, we deduce that the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),\subdiv(a_{0}),a_{1},s(a_{0}),f \circ u, p(a_{1}) \circ \cyl(f \circ g)} %
%
We also have that the following diagram in $\cl{A}$ commutes. \trapeziumstwo[{3,3.5,2,0}]{\cyl(a_{0}),\cyl(a_{2}),a_{0},\cyl^{2}(a_{0}),\cyl(a_{1}),\cyl(a_{0}),a_{1},\cyl(g),\cyl(f),p(a_{1}),p(a_{0}),i_{0}(a_{0}),k,\cyl\big(i_{0}(a_{0})\big),\cyl(k),p\big(\cyl(a_{0})\big)} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),\subdiv(a_{0}),a_{1},s(a_{0}),f \circ u,k \circ i_{0}(a_{0}) \circ p(a_{0})} %
%
Moreover, the following diagram in $\cl{A}$ commutes. \squareabovetriangle{\cyl(a_{0}),\subdiv(a_{0}),a_{1},a_{2},a_{1},r_{0}(a_{0}),u,k,j,f,id} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \pushout{a_{0},\cyl(a_{0}),\cyl(a_{0}),\subdiv(a_{0}),a_{1},i_{0}(a_{0}),r_{0}(a_{0}),i_{1}(a_{0}),r_{1}(a_{0}),k,k \circ i_{0}(a_{0}) \circ p(a_{0}), f \circ u} %
%
Let \ar{\subdiv,\cyl,q_{l}} denote the canonical 2-arrow of $\cl{C}$ of Definition \ref{StrictnessLeftIdentitiesCylinderDefinition}. We have that the following diagram in $\cl{A}$ commutes. \pushout{a_{0},\cyl(a_{0}),\cyl(a_{0}),\subdiv(a_{0}),\cyl(a_{0}),i_{0}(a_{0}),r_{0}(a_{0}),i_{1}(a_{0}),r_{1}(a_{0}),id, i_{0}(a_{0}) \circ p(a_{0}), q_{l}(a_{0})} %
%
Then the following diagram in $\cl{A}$ commutes. \pushout{a_{0},\cyl(a_{0}),\cyl(a_{0}),\subdiv(a_{0}),a_{1},i_{0}(a_{0}),r_{0}(a_{0}),i_{1}(a_{0}),r_{1}(a_{0}),k, k \circ i_{0}(a_{0}) \circ p(a_{0}), k \circ q_{l}(a_{0})} %
%
Appealing to the universal property of $\subdiv(a_{0})$, we deduce that the following diagram in $\cl{A}$ commutes. \tri{\subdiv(a_{0}),\cyl(a_{0}),a_{1},q_{l}(a_{0}),k,f \circ u} %
%
By definition of $l$, the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),\subdiv(a_{0}),a_{2},s(a_{0}),u,l} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),a_{2},a_{1},l,f,k \circ q_{l}(a_{0}) \circ s(a_{0})} %
%
Since $\cylinder$ has strictness of left identities, the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),\subdiv(a_{0}),\cyl(a_{0}),s(a_{0}),q_{l}(a_{0}),id} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes, as required. \tri{\cyl(a_{0}),a_{2},a_{1},l,f,k}
\end{proof}

\begin{cor} \label{TrivialFibrationCylinderIffStrongDeformationRetractionCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, \subdiv, r_{0}, r_{1}, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Suppose that $\cylinder$ has strictness of left identities. 

An arrow \ar{a_{1},a_{0},f} of $\cl{A}$ is a trivial fibration with respect to $\cylinder$ if and only if there is an arrow \ar{a_{0},a_{1},j} of $\cl{A}$, such that $f$ is a retraction of $j$, and such that there exists a homotopy over $a_{0}$ from $jf$ to $id(a_{1})$ with respect to $\cylinder$ and $(f,f)$. \end{cor} 

\begin{proof} Follows immediately from Proposition \ref{DoldOverHomotopiesCylinderProposition} and Proposition \ref{StrongDeformationRetractionIsFibrationProposition}. \end{proof}

\begin{cor} \label{TrivialFibrationCoCylinderIffStrongDeformationRetractionCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$ compatible with $p$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Suppose that $\cylinder$ has strictness of left identities. 

Let $\cocylinder = \big( \cocyl, e_0, e_1, $c$ \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

An arrow \ar{a_{0},a_{1},f} of $\cl{A}$ is a trivial fibration with respect to $\cocylinder$ if and only if it is a strong deformation retraction with respect to $\cocylinder$. \end{cor} 

\begin{proof} Follows immediately from Proposition \ref{StrongDeformationRetractionCoCylinderCharacterisationProposition}, Proposition \ref{ReverseHomotopyIsOverHomotopyProposition}, and Corollary \ref{TrivialFibrationCylinderIffStrongDeformationRetractionCorollary}. \end{proof}

\begin{cor} \label{TrivialCofibrationCylinderIffStrongDeformationRetractionCorollary} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v, \subdiv, r_{0}, r_{1}, s \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v$ compatible with $c$, and a subdivision structure $\big(\subdiv, r_{0}, r_{1}, s \big)$. Suppose that $\cocylinder$ has strictness of left identities. 

Let $\cylinder = \big( \cyl, i_0, i_1,p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

An arrow \ar{a_{0},a_{1},f} of $\cl{A}$ is a trivial cofibration with respect to $\cylinder$ if and only if it admits a strong deformation retraction with respect to $\cylinder$. \end{cor} 

\begin{proof} Follows immediately from Corollary \ref{TrivialFibrationCoCylinderIffStrongDeformationRetractionCorollary} by duality. \end{proof}

\begin{cor} \label{NormallyClovenCofibrationFollowedByTrivialFibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $p$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $p$, and that $\cylinder$ has strictness of identities. Suppose moreover that $\cyl$ preserves mapping cylinders with respect to $\cylinder$. 

Let $\cocylinder = \big( \cocyl, e_0, e_1, $c$ \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$, and let $\big( a^{\cylinder}_{f},d^{0}_{f},d^{1}_{f} \big)$ be a mapping cylinder of $f$ with respect to $\cylinder$. Let \tri{a_{0},a^{\cylinder}_{f},a_{1},j,g,f} denote the corresponding mapping cylinder factorisation of $f$. Then $j$ is a normally cloven cofibration with respect to $\cylinder$, and $g$ is a trivial fibration with respect to $\cocylinder$. \end{cor}

\begin{proof} Follows immediately from Proposition \ref{MappingCylinderFactorisationGivesNormallyClovenCofibrationProposition}, Corollary \ref{MappingCylinderFactorisationStrongDeformationRetractionWithRespectToCoCylinderCorollary}, and Corollary \ref{TrivialFibrationCoCylinderIffStrongDeformationRetractionCorollary}. \end{proof} 

\begin{cor} \label{TrivialCofibrationFollowedByNormallyClovenFibrationCorollary} Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure compatible with $c$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $c$, and that $\cocylinder$ has strictness of identities. Suppose moreover that $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$. 

Let $\cylinder = \big( \cyl, i_0, i_1,p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$.
 
Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$, and let $\big( a^{\cocylinder}_{f},d^{0}_{f},d^{1}_{f} \big)$ be a mapping co-cylinder of $f$ with respect to $\cocylinder$. Let \tri{a_{0},a^{\cocylinder}_{f},a_{1},j,g,f} denote the corresponding mapping co-cylinder factorisation of $f$. 

Then $j$ is a trivial cofibration with respect to $\cylinder$, and $g$ is a normally cloven fibration with respect to $\cocylinder$. \end{cor} 

\begin{proof} Follows immediately from Corollary \ref{NormallyClovenCofibrationFollowedByTrivialFibrationCorollary} by duality. \end{proof}

\begin{cor} \label{CofibrationFollowedByTrivialNormallyClovenFibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $p$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $p$, and that $\cylinder$ has strictness of identities. Suppose moreover that $\cyl$ preserves mapping cylinders with respect to $\cylinder$.

Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure compatible with $c$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $c$, and that $\cocylinder$ has strictness of identities. Suppose moreover that $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$. 

Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$.

Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$. There is an object $a$ of $\cl{A}$, an arrow \ar{a_{0},a,j} of $\cl{A}$ which is a cofibration with respect to $\cylinder$, and an arrow \ar{a,a_{1},g} of $\cl{A}$ which is a trivial normally cloven fibration with respect to $\cocylinder$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a,a_{1},j,g,f} \end{cor}

\begin{proof} Let $\big( a^{\cylinder}_{f},d^{0}_{f},d^{1}_{f} \big)$ be a mapping cylinder of $f$ with respect to $\cylinder$. Let \tri{a_{0},a^{\cylinder}_{f},a_{1},j',g',f} denote the corresponding mapping cylinder factorisation of $f$. By Proposition \ref{MappingCylinderFactorisationGivesNormallyClovenCofibrationProposition}, we have that $j'$ is a cofibration with respect to $\cylinder$. 

Let $\big( a^{\cocylinder}_{g'},d^{0}_{g'},d^{1}_{g'} \big)$ be a mapping co-cylinder of $g'$ with respect to $\cocylinder$. Let \tri{a^{\cylinder}_{f},a^{\cocylinder}_{g'},a_{1},j'',g,g'} denote the corresponding mapping co-cylinder factorisation of $g'$. Let us take $a$ to be $a^{\cocylinder}_{g'}$. By Corollary \ref{TrivialCofibrationFollowedByNormallyClovenFibrationCorollary}, we have that $j''$ is a trivial cofibration with respect to $\cylinder$, and that $g$ is a normally cloven fibration with respect to $\cocylinder$.  

Since $g'$ and $j''$ are homotopy equivalences with respect to $\cylinder$, it follows from Proposition \ref{TwoOutOfThreeHomotopyEquivalencesProposition} that $g$ is a homotopy equivalence with respect to $\cylinder$. Hence, by Proposition \ref{HomotopyEquivalenceCylinderIffHomotopyEquivalenceCoCylinderProposition}, $g$ is a homotopy equivalence with respect to $\cocylinder$. Thus $g$ is a trivial normally cloven fibration with respect to $\cocylinder$.

Let \ar{a_{0},a^{\cocylinder}_{g'},j} denote the arrow $j'' \circ j'$ of $\cl{A}$. Since both $j'$ and $j''$ are cofibrations with respect to $\cylinder$, we conclude that $j$ is a cofibration with respect to $\cylinder$ by Proposition \ref{CompositionOfCofibrationsIsACofibrationProposition}.

\end{proof}

\begin{cor} \label{TrivialNormallyClovenCofibrationFollowedByFibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $p$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $p$, and that $\cylinder$ has strictness of identities. Suppose moreover that $\cyl$ preserves mapping cylinders with respect to $\cylinder$.

Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure compatible with $c$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and a lower right connection structure $\Gamma_{lr}$. Suppose that $\Gamma_{lr}$ is compatible with $c$, and that $\cocylinder$ has strictness of identities. Suppose moreover that $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$. 

Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$.

Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$. There is an object $a$ of $\cl{A}$, an arrow \ar{a_{0},a,j} of $\cl{A}$ which is a trivial normally cloven cofibration with respect to $\cylinder$, and an arrow \ar{a,a_{1},g} of $\cl{A}$ which is a fibration with respect to $\cocylinder$, such that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a,a_{1},j,g,f} \end{cor}

\begin{proof} Follows immediately from Corollary \ref{TrivialNormallyClovenCofibrationFollowedByFibrationCorollary} by duality. \end{proof}

\end{chapter}
