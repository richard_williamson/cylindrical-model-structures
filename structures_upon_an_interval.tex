\begin{chapter}{Structures upon an interval} \label{StructuresUponAnIntervalChapter} 

We define the notion of an interval in a monoidal category $\cl{A}$. We introduce --- exactly in parallel with \ref{StructuresUponACylinderOrACoCylinderChapter} --- structures with which this interval may be able to be equipped. 

An interval $\interval$ in $\cl{A}$ gives rise, under natural hypotheses, to a cylinder $\cylinderinterval$ and a co-cylinder $\cocylinderinterval$ in $\cl{A}$. Structures upon $\interval$ pass to structures upon $\cylinderinterval$ and $\cocylinderinterval$. 

Moreover, $\cylinderinterval$ is left adjoint to $\cocylinderinterval$. Whereas in an abstract setting we work with cylinders and co-cylinders, in practise we often construct a cylinder and co-cylinder via an interval in a monoidal category. 

In parallel with \ref{StructuresUponACylinderOrACoCylinderChapter} once more, we introduce a strictness of left identities hypothesis, a strictness of right identities hypothesis, and a strictness of left inverses hypothesis. If these hypotheses hold for $\interval$, then they hold for $\cylinderinterval$ and $\cocylinderinterval$. 

We refer the reader to \ref{MonoidalCategoryTheoryPreliminariesChapter} for our conventions regarding exponential objects in a monoidal category, and for other preliminary observations to which we shall appeal.

\begin{assum} Let $(\cl{A},\otimes)$ be a monoidal category. Let $1$ denote its unit object, and let $\lambda$ denote its natural isomorphism \ar{- \otimes 1, {-.}} %
%
Let $\alpha$ denote its natural isomorphism \ar{{(- \otimes -) \otimes -}, {- \otimes (- \otimes -).}} \end{assum}

\begin{defn} An {\em interval} in $\cl{A}$ is an object $I$ of $\cl{A}$, together with a pair of arrows \pair{1,I,i_0,i_1} of $\cl{A}$. \end{defn} 

\begin{rmk} We let \ar{\cl{A},\cl{A},{(-)^{1}}} denote the identity functor, by virtue of Remark \ref{UnitExponentiableRemark}. 

We shall frequently implicitly identify the functor \ar[4]{\cl{A},\cl{A},- \otimes 1} with the identity functor, via the natural isomorphism $\lambda$. \end{rmk}

\begin{defn} \label{CylinderAndCoCylinderFromInterval} Let $\interval = \big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. Regarding $\cl{A}$ as an object of the 2-category of categories, we denote by $\cylinderinterval$ the cylinder in $\cl{A}$ defined by the functor \ar[4]{\cl{A},\cl{A},- \otimes I} and the natural transformations \pair[{5,0.75}]{\id_{\cl{A}},{- \otimes I.},- \otimes i_{0}, - \otimes i_{1}} 

If $I$ is exponentiable with respect to $\otimes$, we denote by $\cocylinderinterval$ the co-cylinder in $\cl{A}$ defined by the functor \ar{\cl{A},\cl{A},{(-)^{I}}} and the natural transformations \pair[{4,0.75}]{{(-)^{I}},{\id_{\cl{A}}.},{(-)^{i_{0}}},{(-)^{i_{1}}}} \end{defn}

\begin{prpn} \label{CylinderAndCoCylinderFromIntervalAreAdjointProposition} Let $\interval = \big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. Suppose that $I$ is exponentiable with respect to $\otimes$. Then the cylinder $\cylinderinterval$ in $\cl{A}$ is left adjoint to the co-cylinder $\cocylinderinterval$ in $\cl{A}$. \end{prpn} 

\begin{proof} Let \ar{a_{0} \otimes I,a_{1},h} be an arrow of $\cl{A}$. Since $I$ is exponentiable with respect to $\otimes$, we have a natural isomorphism \ar{{\mathsf{Hom}_{\cl{A}}(- \otimes I, a_{1})},{\mathsf{Hom}_{\cl{A}}(-,(a_{1})^{I}).},\sim} In particular, we have an isomorphism \ar{{\mathsf{Hom}_{\cl{A}}(a_{0} \otimes I,a_{1})},{\mathsf{Hom}_{\cl{A}}(a_{0}, (a_{1})^{I}).},\sim} %
%
Let us denote this isomorphism by $\mathsf{adj}$. 

By definition of \ar[4]{{(a_{1})^{I}},{a_{1},},{(a_{1})^{i_{0}}}} the following diagram in the category of sets commutes. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=10 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| \mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{I}) \& |(1-0)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes I,a_{1}) \\ 
|(0-1)| \mathsf{Hom}_{\cl{A}}(a_{0},a_{1}) \& |(1-1)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes 1, a_{1}) \\
};
	
\draw[->] (0-0) to node[auto] {$\mathsf{adj}^{-1}$} (1-0);
\draw[->] (1-0) to node[auto] {$\mathsf{Hom}_{\cl{A}}(a_{0} \otimes i_{0},a_{1})$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$\mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{i_{0}})$} (0-1);
\draw[<-] (0-1) to node[auto,swap] {$\mathsf{Hom}_{\cl{A}}(\lambda^{-1}(a_{0}),a_{1})$} (1-1);

\end{tikzpicture} 

\end{diagram} %
%
Thus the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{a_{0},a_{0} \otimes I,{(a_{1})^{I}},a_{1},a_{0} \otimes i_{0},h,\mathsf{adj}(h),{(a_{1})^{i_{0}}}} 

Similarly, by definition of \ar[4]{{(a_{1})^{I}},{a_{1},},{(a_{1})^{i_{1}}}} the following diagram in the category of sets commutes. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=10 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| \mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{I}) \& |(1-0)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes I,a_{1}) \\ 
|(0-1)| \mathsf{Hom}_{\cl{A}}(a_{0},a_{1}) \& |(1-1)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes 1, a_{1}) \\
};
	
\draw[->] (0-0) to node[auto] {$\mathsf{adj}^{-1}$} (1-0);
\draw[->] (1-0) to node[auto] {$\mathsf{Hom}_{\cl{A}}(a_{0} \otimes i_{1},a_{1})$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$\mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{i_{1}})$} (0-1);
\draw[<-] (0-1) to node[auto,swap] {$\mathsf{Hom}_{\cl{A}}(\lambda^{-1}(a_{0}),a_{1})$} (1-1);

\end{tikzpicture} 

\end{diagram} Hence the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{a_{0},a_{0} \otimes I,{a_{1}^{I}},a_{1},a_{0} \otimes i_{1},h,\mathsf{adj}(h),{a_{1}^{i_{1}}}} 

\end{proof}

\begin{defn} Let $\interval =\big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. A {\em contraction structure} with respect to $\interval$ is an arrow \ar{I,1,p} of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute. \twotriangles{1,I,1,i_{0},p,id,1,I,1,i_{1},p,id} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. Suppose that $1$ is a final object of $\cl{A}$. This is the case, for example, if the monoidal structure upon $\cl{A}$ is cartesian. Then the canonical arrow \ar{I,1,} of $\cl{A}$ defines a contraction structure with respect to $\interval$. \end{rmk}

\begin{rmk} \label{IntervalWithContractionGivesCylinderWithContractionAndCocylinderWithExpansionRemark} Let $\interval = \big( I, i_0, i_1, p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. Then the natural transformation \ar[4]{- \otimes I, \id_{\cl{A}},- \otimes p} equips the cylinder $\cylinderinterval$ in $\cl{A}$ with a contraction structure. 

If $I$ is exponentiable with respect to $\otimes$, the natural transformation \ar{\id_{\cl{A}},{(-)^{I}},{(-)^{p}}} equips the co-cylinder $\cocylinderinterval$ in $\cl{A}$ with a contraction structure. \end{rmk}

\begin{prpn} \label{AdjunctionBetweenCylinderAndCoCylinderFromIntervalCompatibleWithContractionAndExpansionProposition} Let $\interval = \big( I, i_0, i_1,p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. Suppose that $I$ is exponentiable with respect to $\otimes$. We regard the cylinder $\cylinderinterval$ in $\cl{A}$ as equipped with the contraction structure defined by $- \otimes p$, and regard the co-cylinder $\cocylinderinterval$ in $\cl{A}$ as equipped with the contraction structure defined by $(-)^{p}$. 

Recall by Proposition \ref{CylinderAndCoCylinderFromIntervalAreAdjointProposition} that $\cylinderinterval$ is left adjoint to $\cocylinderinterval$. We moreover have that the adjunction between $- \otimes I$ and $(-)^{I}$ is compatible with $- \otimes p$ and $(-)^{p}$. \end{prpn}

\begin{proof} Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$. Since $I$ is exponentiable with respect to $\otimes$, we have a natural isomorphism \ar{{\mathsf{Hom}_{\cl{A}}(- \otimes I, a_{1})},{\mathsf{Hom}_{\cl{A}}(-,(a_{1})^{I}).},\sim} %
%
In particular, we have an isomorphism \ar{{\mathsf{Hom}_{\cl{A}}(a_{0} \otimes I,a_{1})},{\mathsf{Hom}_{\cl{A}}(a_{0}, (a_{1})^{I}).},\sim} 

Let us denote this isomorphism by $\mathsf{adj}$. By definition of \ar[4]{a_{1},{(a_{1})^{I},},{(a_{1})^{p}}} the following diagram in the category of sets commutes. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=10 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| \mathsf{Hom}_{\cl{A}}(a_{0},a_{1}) \& |(1-0)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes 1,a_{1}) \\ 
|(0-1)| \mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{I}) \& |(1-1)| \mathsf{Hom}_{\cl{A}}(a_{0} \otimes I, a_{1}) \\
};

\draw[->] (0-0) to node[auto] {$\mathsf{Hom}_{\cl{A}}(\lambda(a_{0}),a_{1})$} (1-0);
\draw[->] (1-0) to node[auto] {$\mathsf{Hom}_{\cl{A}}(a_{0} \otimes p,a_{1})$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$\mathsf{Hom}_{\cl{A}}(a_{0},(a_{1})^{p})$} (0-1);

\draw[<-] (0-1) to node[auto,swap] {$\mathsf{adj}$} (1-1);

\end{tikzpicture} 

\end{diagram} Thus the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{1},{(a_{1})^{I}},f,{(a_{1})^{p}},\mathsf{adj}\big( f \circ ( a_{0} \otimes p) \big)} \end{proof}  

\begin{defn}  Let $\interval =\big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. An {\em involution structure} with respect to $\interval$ is an arrow \ar{I,I,v}of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute. \twotriangles{1,I,I,i_{0},v,i_{1},1,I,I,i_{1},v,i_{0}} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, v \big)$ be an interval in $\cl{A}$ equipped with an involution structure $v$. Then the natural transformation \ar[5]{- \otimes I, - \otimes I, - \otimes v} equips the cylinder $\cylinderinterval$ in $\cl{A}$ with an involution structure. 

If $I$ is exponentiable with respect to $\otimes$, the natural transformation \ar{{(-)^{I}},{(-)^{I}},{(-)^{v}}} equips the co-cylinder $\cocylinderinterval$ in $\cl{A}$ with an involution structure. \end{rmk}

\begin{defn} Let $\interval = \big( I, i_{0}, i_{1}, p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. An involution structure $v$ with respect to $\interval$ is {\em compatible with $p$} if the following diagram in $\cl{A}$ commutes. \tri{I,I,1,v,p,p} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_{0}, i_{1}, p, v \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and an involution structure $v$ compatible with $p$. Then the involution structure $- \otimes v$ with respect to the cylinder $\cylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $- \otimes p$.

If $I$ is exponentiable with respect to $\otimes$, the involution structure $(-)^{v}$ with respect to the co-cylinder $\cocylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $(-)^{p}$. \end{rmk}

\begin{defn} Let $\interval = \big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. A {\em subdivision structure} with respect to $\interval$ is an object $S$ of $\cl{A}$, together with a pair of arrows \pair{I,S,r_{0},r_{1}} of $\cl{A}$, such that the square \sq{1,I,I,S,i_{0},r_{0},i_{1},r_{1}} in $\cl{A}$ is co-cartesian, and an arrow \ar{I,S,s} of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute. \twosq{1,I,I,S,i_{0},s,i_{0},r_{1},1,I,I,S,i_{1},s,i_{1},r_{0}} \end{defn}

\begin{requirement} \label{SubdivisionRequirement} Let $\interval = \big( I, i_{0}, i_{1} \big)$ be an interval in $\cl{A}$, and let $\big( S, r_{0}, r_{1}, s \big)$ be a subdivision structure with respect to $\interval$. Then for any object $a$ of $\cl{A}$, the square \sq{a,a \otimes I, a \otimes I, a \otimes S, a \otimes i_{0}, a \otimes r_{0}, a \otimes i_{1}, a \otimes r_{1}} in $\cl{A}$ is co-cartesian. \end{requirement}

\begin{rmk} \label{SubdivisionRequirementRemark} Requirement \ref{SubdivisionRequirement} is satisfied whenever the monoidal structure on $\cl{A}$ is closed. It is also satisfied, for example, by the usual interval in the category of topological spaces, equipped with its cartesian monoidal structure. This monoidal structure is not closed. 

Since by Proposition \ref{CylinderAndCoCylinderFromIntervalAreAdjointProposition} we have that $\cylinderinterval$ is left adjoint to $\cocylinderinterval$, Requirement \ref{SubdivisionRequirement} is equivalent to the dual requirement that, for any object $a$ of $\cl{A}$, the square \sq{a^{S},a^{I},a^{I},a,a^{r_{0}},a^{i_{0}},a^{r_{1}},a^{i_{1}}} in $\cl{A}$ is cartesian. \end{rmk}

\begin{rmk} Let $\interval = \big( I, i_{0}, i_{1} \big)$ be an interval in $\cl{A}$, and let $\big( S, r_{0}, r_{1}, s \big)$ be a subdivision structure with respect to $\interval$, such that Requirement \ref{SubdivisionRequirement} holds. Then the functor \ar[4]{\cl{A},\cl{A},- \otimes S} and the natural transformations \pair[{5,0.75}]{- \otimes I, - \otimes S, - \otimes r_{0}, - \otimes r_{1}} and \ar[5]{- \otimes I, - \otimes S, - \otimes s} define a subdivision structure with respect to the cylinder $\cylinderinterval$ in $\cl{A}$. Moreover, $- \otimes I$ preserves subdivision with respect to $\cylinderinterval$ and $\big(- \otimes S, - \otimes r_{0}, - \otimes r_{1}, - \otimes s \big)$. 

If both $I$ and $S$ are exponentiable with respect to $\otimes$ then the functor \ar[4]{\cl{A},\cl{A},{(-)^{S}}} and the natural transformations \pair[{5,0.75}]{{(-)^{S}}, {(-)^{I}}, {(-)^{r_{0}}}, {(-)^{r_{1}}}} and \ar[5]{{(-)^{S}}, {(-)^{I}}, {(-)^{s}}} defines a subdivision structure with respect to the co-cylinder $\cocylinderinterval$ in $\cl{A}$. Moreover $(-)^{I}$ preserves subdivision with respect to $\cocylinderinterval$ and $\big( {(-)^{S}}, {(-)^{r_{0}}}, {(-)^{r_{1}}}, {(-)^{s}} \big)$.  \end{rmk} 

\begin{defn} Let $\interval = \big( I, i_{0}, i_{1}, p, S, r_{0}, r_{1}, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$. Let \ar{S,1,\overline{p}} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{1,I,I,S,1,i_0,r_0,i_1,r_1,p,p,\overline{p}} %
%
The subdivision structure $\big( S, r_{0}, r_{1}, s \big)$ is {\em compatible with $p$} if the following diagram in $\cl{A}$ commutes. \tri{I,S,1,s,\overline{p},p} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_{0}, i_{1}, p, S, r_{0}, r_{1}, s\big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$. Suppose that Requirement \ref{SubdivisionRequirement} holds. Then the subdivision structure $\big( - \otimes S, - \otimes r_{0}, i- \otimes r_{1}, - \otimes s \big)$ with respect to the cylinder $\cylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $- \otimes p$. 

If both $I$ and $S$ are exponentiable with respect to $\otimes$, then the subdivision structure $\big( {(-)^{S}}, {(-)^{r_{0}}}, {(-)^{r_{1}}}, {(-)^{s}} \big)$ with respect to the co-cylinder $\cocylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $(-)^{p}$. \end{rmk}

\begin{notn} Let $\big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. We denote by $I^{2}$ the object $I \otimes I$ of $\cl{A}$. \end{notn}

\begin{rmk} Let $\big( I, i_0, i_1 \big)$ be an interval in $\cl{A}$. We shall frequently implicitly identify the functor $(- \otimes I) \otimes I$ with the functor $- \otimes I^{2}$, via the natural isomorphism $\alpha$. Similarly, we shall frequently implicitly identify the functor  $(- \otimes I) \otimes 1$ with the functor $- \otimes (I \otimes 1)$, via $\alpha$. \end{rmk} 

\begin{defn} Let $\interval = \big( I, i_0, i_1, p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. An {\em upper left connection structure} with respect to $\interval$ is an arrow \ar{I^{2},I,\Gamma_{ul}}of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute.  \twotriangles[{4,3,0}]{I,I^{2},I,i_{0} \otimes I, \Gamma_{ul},id,I,I^{2},I,I \otimes i_{0},\Gamma_{ul},id} %
%
\twosq[{4,3,0}]{I,I^{2},1,I,i_{1} \otimes I,\Gamma_{ul},p,i_{1},I,I^{2},1,I,I \otimes i_{1},\Gamma_{ul},p,i_{1}} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, \Gamma_{ul} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and an upper left connection structure $\Gamma_{ul}$. Let us regard the cylinder $\cylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $- \otimes p$, Then the natural transformation \ar[5]{- \otimes I^{2}, - \otimes I, - \otimes \Gamma_{ul}} equips $\cylinderinterval$ with an upper left connection structure. 

Suppose that $I$ is exponentiable with respect to $\otimes$. Let us regard the co-cylinder $\cocylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $(-)^{p}$. Then the natural transformation \ar[4]{{(-)^{I}},{(-)^{I^{2}}},{(-)^{\Gamma_{ul}}}} equips $\cocylinderinterval$ with an upper left connection structure. \end{rmk}

\begin{defn} Let $\interval = \big( I, i_0, i_1, p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. A {\em lower right connection structure} with respect to $\interval$ is an arrow \ar{I^{2},I,\Gamma_{lr}} of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute.  \twotriangles[{4,3,0}]{I,I^{2},I,i_{1} \otimes I, \Gamma_{lr},id,I,I^{2},I,I \otimes i_{1},\Gamma_{lr},id} %
%
\twosq[{4,3,0}]{I,I^{2},1,I,i_{0} \otimes I,\Gamma_{lr},p,i_{0},I,I^{2},1,I,I \otimes i_{0},\Gamma_{lr},p,i_{0}} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, \Gamma_{lr} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and a lower right connection structure $\Gamma_{lr}$. Let us regard the cylinder $\cylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $- \otimes p$. Then the natural transformation \ar[5]{- \otimes I^{2}, - \otimes I, - \otimes \Gamma_{lr}} equips $\cylinderinterval$ with a lower right connection structure. 

Suppose that $I$ is exponentiable with respect to $\otimes$. Let us regard the co-cylinder $\cocylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $(-)^{p}$. Then the natural transformation \ar[4]{{(-)^{I}},{(-)^{I^{2}}},{(-)^{\Gamma_{lr}}}} equips $\cocylinderinterval$ with a lower right connection structure. \end{rmk}

\begin{defn} Let $\interval = \big( I, i_{0}, i_{1}, p \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$. A lower right connection structure $\Gamma_{lr}$ with respect to $\interval$ is {\em compatible with $p$} if the following diagram in $\cl{A}$ commutes. \sq{I^{2},I,I,1,\Gamma_{lr},p,I \otimes p,p} \end{defn} 

\begin{rmk} Let $\interval = \big( I, i_{0}, i_{1}, p, \Gamma_{lr} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and a lower right connection structure $\Gamma_{lr}$. If $\Gamma_{lr}$ is compatible with $p$, then the lower right connection structure $- \otimes \Gamma_{lr}$ with respect to the cylinder $\cylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $- \otimes p$. 

Suppose that $I$ is exponentiable with respect to $\otimes$. Then the lower right connection structure $(-)^{\Gamma_{lr}}$ with respect to the co-cylinder $\cocylinderinterval$ in $\cl{A}$ is compatible with the contraction structure $(-)^{p}$. \end{rmk} 

\begin{rmk} We shall not need to consider compatibility of an upper left connection structure upon an interval with a contraction structure, or compatibility of an upper right connection structure upon an interval, which we shall define next, with a contraction structure. \end{rmk}

\begin{defn} Let $\interval = \big( I, i_0, i_1, p, v \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and an involution structure $v$. An {\em upper right connection structure} with respect to $\interval$ is an arrow \ar{I^{2},I,\Gamma_{ur}} of $\cl{A}$, such that the following diagrams in $\cl{A}$ commute. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=5 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| I \& |(1-0)| I^{2} \&[2em] |(2-0)| I \& |(3-0)| I^{2} \\ 
          \& |(1-1)| I     \&                \& |(3-1)| I     \\
|(0-2)| I \& |(1-2)| I^{2} \&      |(2-2)| I \& |(3-2)| I^{2} \\
|(0-3)| 1 \& |(1-3)| I     \&      |(2-3)| 1 \& |(3-3)| I     \\      
};
	
\draw[->] (0-0) to node[auto] {$I \otimes i_{0}$} (1-0);
\draw[->] (1-0) to node[auto] {$\Gamma_{ur}$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$id$} (1-1);
\draw[->] (2-0) to node[auto] {$i_{1} \otimes I$} (3-0);
\draw[->] (3-0) to node[auto] {$\Gamma_{ur}$} (3-1);
\draw[->] (2-0) to node[auto,swap] {$v$} (3-1);
\draw[->] (0-2) to node[auto] {$i_{0} \otimes I$} (1-2);
\draw[->] (1-2) to node[auto] {$\Gamma_{ur}$} (1-3);
\draw[->] (0-2) to node[auto,swap] {$p$} (0-3);
\draw[->] (0-3) to node[auto,swap] {$i_{0}$} (1-3);
\draw[->] (2-2) to node[auto] {$I \otimes i_{1}$} (3-2);
\draw[->] (3-2) to node[auto] {$\Gamma_{ur}$} (3-3);
\draw[->] (2-2) to node[auto,swap] {$p$} (2-3);
\draw[->] (2-3) to node[auto,swap] {$i_{0}$} (3-3);

\end{tikzpicture} 

\end{diagram} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, v, \Gamma_{ur} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, and an upper right connection structure $\Gamma_{ur}$. Let us regard the cylinder $\cylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $- \otimes p$ and the involution structure $- \otimes v$. Then the natural transformation \ar[5]{- \otimes I^{2}, - \otimes I, - \otimes \Gamma_{ur}} equips $\cylinderinterval$ in $\cl{A}$ with an upper right connection structure. 

Suppose that $I$ is exponentiable with respect to $\otimes$. Let us regard the co-cylinder $\cocylinderinterval$ in $\cl{A}$ as equipped with the contraction structure $(-)^{p}$ and the involution structure $(-)^{v}$. Then the natural transformation \ar[4]{{(-)^{I}},{(-)^{I^{2}}},{(-)^{\Gamma_{ur}}}} equips $\cocylinderinterval$ in $\cl{A}$ with an upper right connection structure. \end{rmk}

\begin{rmk} Analogously, one can define a {\em lower left connection structure} with respect to an interval. Everything concerning upper and lower right connections below can equally be carried out for upper and lower left connections. \end{rmk}

\begin{defn} \label{RightConnectionsIntervalCompatibilityDefinition} Let $\interval = \big( I, i_0, i_1, p, v, S, r_0, r_1, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. Let \ar{I \otimes S,I,x} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout[{4,3,-1}]{I,I^{2},I^{2},I \otimes S,I,I \otimes i_{0},I \otimes r_0,I \otimes i_1,I \otimes r_1, \Gamma_{lr},\Gamma_{ur},x} Then $\Gamma_{lr}$ and $\Gamma_{ur}$ are {\em compatible with $\big( S, r_{0}, r_{1}, s \big)$} if the following diagram in $\cl{A}$ commutes. \tri[{4,3}]{I^{2},I \otimes S,I,I \otimes s,x,I \otimes p} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, v, S, r_0, r_1, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with $\big( S, r_{0}, r_{1}, s \big)$, and that Requirement \ref{SubdivisionRequirement} holds. 

Then the right connections $- \otimes \Gamma_{lr}$ and $- \otimes \Gamma_{ur}$ are compatible with the subdivision structure $\big( - \otimes S, - \otimes r_{0}, - \otimes r_{1}, - \otimes s \big)$ upon the cylinder $\cylinderinterval$ in $\cl{A}$ equipped with the contraction structure $- \otimes p$. 

Suppose that both $I$ and $S$ are exponentiable with respect to $\otimes$. Then the right connections $(-)^{\Gamma_{lr}}$ and $(-)^{\Gamma_{ur}}$ are compatible with the subdivision structure $\big( (-)^{S}, (-)^{r_{0}}, (-)^{r_{1}}, (-)^{s} \big)$ upon the co-cylinder $\cocylinderinterval$ in $\cl{A}$ equipped with the contraction structure $(-)^{p}$. \end{rmk}

\begin{defn} \label{StrictnessLeftIdentitiesIntervalDefinition} Let $\interval = \big( I, i_0, i_1, p, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Let \ar{S,I,q_{l}} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{1,I,I,S,I,i_{0},r_{0},i_{1},r_{1},id,i_{0} \circ p,q_{l}} %
%
Then $\interval$ has {\em strictness of left identities} if the following diagram in $\cl{A}$ commutes. \tri{I,S,I,s,q_{l},id} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Suppose that Requirement \ref{SubdivisionRequirement} holds and that $\interval$ has strictness of left identities. 

Then the cylinder $\cylinderinterval$ in $\cl{A}$ equipped with the contraction structure $- \otimes p$ and the subdivision structure $\big( - \otimes S, - \otimes r_{0}, - \otimes r_{1}, - \otimes s \big)$ has strictness of left identities. 

If both $I$ and $S$ are exponentiable with respect to $\otimes$, then the co-cylinder $\cocylinderinterval$ in $\cl{A}$ equipped with the contraction structure $(-)^{p}$ and the subdivision structure $\big( (-)^{S}, (-)^{r_{0}}, (-)^{r_{1}}, (-)^{s} \big)$ has strictness of left identities. \end{rmk} 

\begin{defn} \label{StrictnessRightIdentitiesIntervalDefinition} Let $\interval = \big( I, i_0, i_1, p, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$, and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Let \ar{S,I,q_{r}} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{1,I,I,S,I,i_{0},r_{0},i_{1},r_{1},i_{1} \circ p,id,q_{r}} % 
%
Then $\interval$ has {\em strictness of right identities} if the following diagram in $\cl{A}$ commutes. \tri{I,S,I,s,q_{r},id} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, p, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Suppose that Requirement \ref{SubdivisionRequirement} holds, and that $\interval$ has strictness of right identities. 

Then the cylinder $\cylinderinterval$ in $\cl{A}$ equipped with the contraction structure $- \otimes p$ and the subdivision structure $\big( - \otimes S, - \otimes r_{0}, - \otimes r_{1}, - \otimes s \big)$ has strictness of right identities. 

If both $I$ and $S$ are exponentiable with respect to $\otimes$, then the co-cylinder $\cocylinderinterval$ in $\cl{A}$ equipped with the contraction structure $(-)^{p}$ and the subdivision structure $\big( (-)^{S}, (-)^{r_{0}}, (-)^{r_{1}}, (-)^{s} \big)$ has strictness of right identities. \end{rmk} 

\begin{defn}  Let $\interval = \big( I, i_0, i_1, p, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with a contraction structure $p$ and a subdivision structure $\big( S, r_{0}, r_{1}, S \big)$. Then $\interval$ has {\em strictness of identities} if it has both strictness of left identities and strictness of right identities. \end{defn}

\begin{defn} \label{StrictnessLeftInversesDefinition} Let $\interval = \big( I, i_0, i_1, v, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with an involution structure $v$, and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Let \ar{S,I,w} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{1,I,I,S,I,i_{0},r_{0},i_{1},r_{1},id,v,w} Then $\interval$ has {\em strictness of left inverses} if the following diagram in $\cl{A}$ commutes. \sq{I,S,1,I,s,w,p,i_1} \end{defn}

\begin{rmk} Let $\interval = \big( I, i_0, i_1, v, S, r_0, r_1, s \big)$ be an interval in $\cl{A}$ equipped with an involution structure $v$ and a subdivision structure $\big( S, r_{0}, r_{1}, s \big)$. Suppose that Requirement \ref{SubdivisionRequirement} holds. Then the cylinder $\cylinderinterval$ in $\cl{A}$ equipped with the involution structure $- \otimes v$ and the subdivision structure $\big( - \otimes S, - \otimes r_{0}, - \otimes r_{1}, - \otimes s \big)$ has strictness of left inverses. 

If both $I$ and $S$ are exponentiable with respect to $\otimes$, then the co-cylinder $\cocylinderinterval$ in $\cl{A}$ equipped with the involution structure $(-)^{v}$ and the subdivision structure  $\big( (-)^{S}, (-)^{r_{0}}, (-)^{r_{1}}, (-)^{s} \big)$ has strictness of left inverses. \end{rmk}

\begin{rmk} We shall not need to consider strictness of right inverses. \end{rmk}

\end{chapter}
