\begin{chapter}{Lifting axioms} \label{LiftingAxiomsChapter}

Let $\cylinder$ be a cylinder in a formal category $\cl{A}$, and let $\cocylinder$ be a co-cylinder in $\cl{A}$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$. We prove that if an arrow $j$ of $\cl{A}$ is a cofibration, then the canonical arrow $m^{\cylinder}_{j}$ of $\cl{A}$ defined in \ref{MappingCylindersAndMappingCoCylindersChapter} admits a strong deformation retraction. For this we assume, for the first time, that our cylinder is equipped with upper and lower right connection structures, which we require to be compatible with subdivision. 

For topological spaces, the fact that $m^{\cylinder}_{j}$ admits a strong deformation retraction if $j$ is a cofibration is due to Str{\o}m, proven in \S{2} of the paper \cite{StromNoteOnCofibrations}. Str{\o}m's argument is, however, quite different to ours. It relies on the fact that the homotopy theory of topological spaces is defined with respect to a cartesian monoidal structure.

Next, we prove that a normally cloven fibration has the right lifting property with respect to arrows admitting a strong deformation retraction. We deduce that normally cloven fibrations have the covering homotopy extension property with respect to cofibrations. 

In a similar way, we prove that trivial normally cloven fibrations have the right lifting property with respect to cofibrations. Dualising, we deduce that fibrations have the right lifting property with respect to trivial normally cloven cofibrations, and that trivial fibrations have the right lifting property with respect to normally cloven cofibrations. 

\begin{assum} Let $\cl{C}$ be a 2-category with a final object. Suppose that pushouts and pullbacks of 2-arrows of $\cl{C}$ give rise to pushouts and pullbacks in formal categories, in the sense of Definition \ref{PushoutsPullbacks2ArrowsArePushoutsPullbacksInFormalCategoriesTerminology}. Let $\cl{A}$ be an object of $\cl{C}$. As before, we view $\cl{A}$ as a formal category, writing of objects and arrows of $\cl{A}$. \end{assum} 

\begin{prpn} \label{RetractionIsStrongDeformationRetractionProposition} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and that $\cyl$ preserves mapping cylinders with respect to $\cylinder$. 

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which is a cofibration with respect to $\cylinder$, and suppose that $\big( a^{\cylinder}_{j}, d^{0}_{j}, d^{1}_{j} \big)$ defines a mapping cylinder of $j$ with respect to $\cylinder$. Let \ar{a^{\cylinder}_{j},\cyl(a_{1}),m^{\cylinder}_{j}} denote the corresponding canonical arrow of $\cl{A}$ of Notation \ref{CanonicalArrowFromMappingCylinderToCylinderDefinition}. Let \ar{\cyl(a_{1}),a^{\cylinder}_{j},r^{\cylinder}_{j}} be the arrow of $\cl{A}$ of Proposition \ref{RetractionToMappingCylinderProposition}. %
%
Then $r^{\cylinder}_{j}$ is a strong deformation retraction of $m^{\cylinder}_{j}$ with respect to $\cylinder$. \end{prpn}

\begin{proof} By Proposition \ref{RetractionToMappingCylinderProposition}, we have that $r^{\cylinder}_{j}$ is a retraction of $m^{\cylinder}_{j}$. It remains to prove that there is a homotopy \ar{\cyl^{2}(a_{1}),\cyl(a_{1}),\sigma} from $m^{\cylinder}_{j} \circ r^{\cylinder}_{j}$ to $id\big( \cyl(a_{1}) \big)$ with respect to $\cylinder$, such that the following diagram in $\cl{A}$ commutes. \sq{\cyl\big(a^{\cylinder}_{j}\big),a^{\cylinder}_{j},\cyl^{2}(a_{1}),\cyl(a_{1}),p\big(a^{\cylinder}_{j}\big),m^{\cylinder}_{j},\cyl\big(m^{\cylinder}_{j}\big),\sigma} 

Let us construct $\sigma$. We have that the following diagram in $\cl{A}$ commutes. \squarewithdiagonal{a_{1},\cyl(a_{1}),\cyl(a_{1}),a^{\cylinder}_{j},i_{0}(a_{1}),r^{\cylinder}_{j},i_{0}(a_{1}),m^{\cylinder}_{j},d^{1}_{j}} %
%
By definition of $\Gamma_{ur}$ as an upper right connection structure, we also have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),a_{1},\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),\Gamma_{ur}(a_{1}),p(a_{1}),i_{0}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),a_{1},\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1}),p(a_{1}),i_{0}(a_{1})} %
%
By definition of $\Gamma_{lr}$ as a lower right connection structure, the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),a_{1},\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\Gamma_{ur}(a_{1}),p(a_{1}),i_{0}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{9,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\Gamma_{lr}(a_{1}),i_{1}\big(\cyl(a_{1})\big),m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1})} %
%
We define $\sigma$ to be the arrow \ar[15]{\cyl^{2}(a_{1}),\cyl(a_{1}),\big( m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1}) \big) + \Gamma_{lr}(a_{1})} of $\cl{A}$. 

Let us first prove that the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}), \cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\sigma,m^{\cylinder}_{j} \circ r^{\cylinder}_{j}} %
%
By definition of $\Gamma_{ur}$ as an upper right connection structure, the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\Gamma_{ur}(a_{1}),id} %
%
Thus the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1}),m^{\cylinder}_{j} \circ r^{\cylinder}_{j}} %
%
We also have that the following diagram in $\cl{A}$ commutes. \sq[{9,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\sigma,i_{0}\big(\cyl(a_{1})\big),m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes, as required. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}), \cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),\sigma,m^{\cylinder}_{j} \circ r^{\cylinder}_{j}} 

Next, let us prove that the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),\sigma,id} %
%
By definition of $\Gamma_{lr}$ as a lower right connection structure, we have that the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),\Gamma_{lr}(a_{1}),id} %
%
We also have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),\sigma,i_{1}\big(\cyl(a_{1})\big),\Gamma_{lr}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes, as required. \tri[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl(a_{1}),i_{1}\big(\cyl(a_{1})\big),\Gamma_{lr}(a_{1}),id} 

Let us now prove that the following diagram in $\cl{A}$ commutes. \sq{\cyl\big(a^{\cylinder}_{j}\big),a^{\cylinder}_{j},\cyl^{2}(a_{1}),\cyl(a_{1}),p\big(a^{\cylinder}_{j}\big),m^{\cylinder}_{j},\cyl\big(m^{\cylinder}_{j}\big),\sigma} %
%
Let \ar{\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),u} denote the canonical arrow of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout[{5,3,0}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),i_{0}\big(\cyl(a_{1})\big),r_{0}\big(\cyl(a_{1})\big),i_{1}\big(\cyl(a_{1})\big),r_{1}\big(\cyl(a_{1})\big),\Gamma_{lr}(a_{1}),m^{\cylinder}_{j} \circ r^{\cylinder}_{j} \circ \Gamma_{ur}(a_{1}),u} %
%
Let \ar{\subdiv,\id,\overline{p}} denote the canonical 2-arrow of $\cl{C}$ of Definition \ref{SubdivisionCompatibleWithContractionDefinition}. We claim that the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl(a_{1}),s(a_{1}),u \circ \subdiv\big(i_{0}(a_{1})\big),p(a_{1}),i_{0}(a_{1})} 

By definition of $\Gamma_{lr}(a_{1})$ as a lower right connection structure, the following diagram in $\cl{A}$ commutes. \sq[{5,3}] {\cyl(a_{1}),\cyl^{2}(a_{1}),a_{1},\cyl(a_{1}),\cyl\big(i_{0}(a_{1})\big),\Gamma_{ur}(a_{1}),p(a_{1}),i_{0}(a_{1})} %
%
We also have that the following diagram in $\cl{A}$ commutes. \squareabovetriangle[{5,3,0}]{\cyl(a_{1}),\subdiv(a_{1}),\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),r_{0}(a_{1}),\subdiv\big(i_{0}(a_{1})\big),\cyl\big(i_{0}(a_{1})\big),r_{0}\big(\cyl(a_{1})\big),u,\Gamma_{lr}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl(a_{1}),r_{0}(a_{1}),u \circ \subdiv\big(i_{0}(a_{1})\big),p(a_{1}),i_{0}(a_{1})}
%
By definition of $\Gamma_{ur}(a_{1})$ as an upper right connection structure, the following diagram in $\cl{A}$ commutes. \sq[{5,3}] {\cyl(a_{1}),\cyl^{2}(a_{1}),a_{1},\cyl(a_{1}),\cyl\big(i_{0}(a_{1})\big),\Gamma_{ur}(a_{1}),p(a_{1}),i_{0}(a_{1})} %
%
Thus we have that the following diagram in $\cl{A}$ commutes. \trapeziumstwo[{3,3,2,0}]{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),\cyl(a_{1}),r_{1}(a_{1}),\subdiv\big(i_{0}(a_{1})\big),u,p(a_{1}),i_{0}(a_{1}),m^{\cylinder}_{j} \circ r^{\cylinder}_{j},\cyl\big(i_{0}(a_{1})\big),r_{1}\big(\cyl(a_{1})\big),\Gamma_{ur}(a_{1})} %
%
Earlier in the proof, we observed that the following diagram in $\cl{A}$ commutes. \tri{a_{1},\cyl(a_{1}),\cyl(a_{1}),i_{0}(a_{1}),m^{\cylinder}_{j} \circ r^{\cylinder}_{j},i_{0}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl(a_{1}),r_{1}(a_{1}),u \circ \subdiv\big(i_{0}(a_{1})\big),p(a_{1}),i_{0}(a_{1})} %
%
We have now shown that the following diagram in $\cl{A}$ commutes. \pushout{a_{1},\cyl(a_{1}),\cyl(a_{1}),\subdiv(a_{1}),\cyl(a_{1}),i_{0}(a_{1}),r_{0}(a_{1}),i_{1}(a_{1}),r_{1}(a_{1}),i_{0}(a_{1}) \circ p(a_{1}),i_{0}(a_{1}) \circ p(a_{1}), u \circ \subdiv\big(i_{0}(a_{1})\big)} %
%
By definition of $\overline{p}$, the following diagram in $\cl{A}$ commutes. \pushout{a_{1},\cyl(a_{1}),\cyl(a_{1}),\subdiv(a_{1}),a_{1},i_{0}(a_{1}),r_{0}(a_{1}),i_{1}(a_{1}),r_{1}(a_{1}),p(a_{1}), p(a_{1}), \overline{p}(a_{1})} %
%
Thus the following diagram in $\cl{A}$ commutes. \pushout{a_{1},\cyl(a_{1}),\cyl(a_{1}),\subdiv(a_{1}),\cyl(a_{1}),i_{0}(a_{1}),r_{0}(a_{1}),i_{1}(a_{1}),r_{1}(a_{1}),i_{0}(a_{1}) \circ p(a_{1}),i_{0}(a_{1}) \circ p(a_{1}), i_{0}(a_{1}) \circ \overline{p}(a_{1})} %
%
Appealing to the universal property of $\subdiv(a_{1})$, we deduce that the following diagram in $\cl{A}$ commutes.\sq[{5,3}]{\subdiv(a_{1}),\subdiv\big(\cyl(a_{1})\big),a_{1},\cyl(a_{1}),\subdiv\big(i_{0}(a_{1})\big),u,\overline{p}(a_{1}),i_{0}(a_{1})} %
%
Since the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ is compatible with $p$, we also have that the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{1}),\subdiv(a_{1}),a_{1},s(a_{1}),\overline{p}(a_{1}),p(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes, completing the proof of the claim. \squarewithdiagonaltwo[{4,3.5}]{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl(a_{1}),s(a_{1}),u \circ \subdiv\big(i_{0}(a_{1})\big),p(a_{1}),i_{0}(a_{1}),\overline{p}(a_{1})} 

We also claim that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl(a_{1}),s\big(\cyl(a_{0})\big),u \circ \subdiv\big(\cyl(j)\big),p\big(\cyl(a_{0})\big),\cyl(j)} %
%
We have that the following diagram in $\cl{A}$ commutes. \trapeziumstwo[{3,3,2,0}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),\cyl(a_{1}),r_{1}\big(\cyl(a_{0})\big),\subdiv\big(\cyl(j)\big),u,\Gamma_{ur}(a_{0}),\cyl(j),m^{\cylinder}_{j} \circ r^{\cylinder}_{j},\cyl^{2}(j),r_{1}\big(\cyl(a_{1})\big),\Gamma_{ur}(a_{1})} %
% 
The following diagram in $\cl{A}$ also commutes. \squarewithdiagonal{\cyl(a_{0}),\cyl(a_{1}),\cyl(a_{1}),a^{\cylinder}_{j},\cyl(j),r^{\cylinder}_{j},\cyl(j),m^{\cylinder}_{j},d^{0}_{j}} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl(a_{1}),r_{1}\big(\cyl(a_{0})\big),u \circ \subdiv\big(\cyl(j)\big),\Gamma_{ur}(a_{0}),\cyl(j)} %
%
The following diagram in $\cl{A}$ commutes. \squareabovetriangle[{5,3,0}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),r_{0}\big(\cyl(a_{0})\big),\subdiv\big(\cyl(j)\big),\cyl^{2}(j),r_{0}\big(\cyl(a_{1})\big),u,\Gamma_{lr}(a_{1})} %
%
The following diagram in $\cl{A}$ also commutes. \sq{\cyl^{2}(a_{0}),\cyl^{2}(a_{1}),\cyl(a_{0}),\cyl(a_{1}),\cyl^{2}(j),\Gamma_{lr}(a_{1}),\Gamma_{lr}(a_{0}),\cyl(j)} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl(a_{1}),r_{0}\big(\cyl(a_{0})\big),u \circ \subdiv\big(\cyl(j)\big),\Gamma_{lr}(a_{0}),\cyl(j)} %
%
We have now shown that the following diagram in $\cl{A}$ commutes. \pushout[{5,3,0}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{1}),i_{0}\big(\cyl(a_{0})\big),r_{0}\big(\cyl(a_{0})\big),i_{1}\big(\cyl(a_{0})\big),r_{1}\big(\cyl(a_{0})\big),\cyl(j) \circ \Gamma_{lr}(a_{0}),\cyl(j) \circ \Gamma_{ur}(a_{0}), u \circ \subdiv\big(\cyl(j)\big)} %
%
Let \ar{\subdiv \circ \cyl,\cyl,x} denote the canonical 2-arrow of $\cl{C}$ of Definition \ref{RightConnectionsCylinderCompatibilityDefinition}. By definition of $x$, the following diagram in $\cl{A}$ commutes. \pushout[{5,3,0}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),i_{0}\big(\cyl(a_{0})\big),r_{0}\big(\cyl(a_{0})\big),i_{1}\big(\cyl(a_{0})\big),r_{1}\big(\cyl(a_{0})\big),\Gamma_{lr}(a_{0}),\Gamma_{ur}(a_{0}),x(a_{0})} %
%
Thus the following diagram in $\cl{A}$ commutes. \pushout[{5,3,0}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{1}),i_{0}\big(\cyl(a_{0})\big),r_{0}\big(\cyl(a_{0})\big),i_{1}\big(\cyl(a_{0})\big),r_{1}\big(\cyl(a_{0})\big),\cyl(j) \circ \Gamma_{lr}(a_{0}),\cyl(j) \circ \Gamma_{ur}(a_{0}),\cyl(j) \circ x(a_{0})} %
%
Appealing to the universal property of $\subdiv\big(\cyl(a_{0})\big)$, we deduce that the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{\subdiv\big(\cyl(a_{0})\big),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{0}),\cyl(a_{1}),\subdiv\big(\cyl(j)\big),u,x(a_{0}),\cyl(j)} % 
%
Since $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv,r_{0}, r_{1}, s \big)$, we also have that the following diagram in $\cl{A}$ commutes. \tri[{5,4}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),s\big(\cyl(a_{0})\big),x(a_{0}),p\big(\cyl(a_{0})\big)} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes, completing the proof of the claim. \squarewithdiagonaltwo[{5,3}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl(a_{1}),s\big(\cyl(a_{0})\big),u \circ \subdiv(\cyl(j)\big),p\big(\cyl(a_{0})\big), \cyl(j), x(a_{0})} 

Next, we claim that the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{\cyl^{2}(a_{0}),\cyl\big(a^{\cylinder}_{j}\big),\cyl(a_{0}),\cyl(a_{1}),\cyl(d^{0}_{j}),\sigma \circ \cyl\big(m^{\cylinder}_{j}\big),p\big(\cyl(a_{0})\big),\cyl(j)} %
%
We have that the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),a^{\cylinder}_{j},\cyl(a_{1}),d^{0}_{j},m^{\cylinder}_{j},\cyl(j)} %
%
Thus the following diagram in $\cl{A}$ commutes. \tri[{4,3}]{\cyl^{2}(a_{0}),\cyl\big(a^{\cylinder}_{j}\big),\cyl^{2}(a_{1}),\cyl(d^{0}_{j}),\cyl\big(m^{\cylinder}_{j}\big),\cyl^{2}(j)} %
%
The following diagram in $\cl{A}$ also commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{0})\big),\subdiv\big(\cyl(a_{1})\big),\cyl^{2}(j),s\big(\cyl(a_{1})\big),s\big(\cyl(a_{0})\big),\subdiv\big(\cyl(j)\big)} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\cyl\big(a^{\cylinder}_{j}\big),\subdiv\big(\cyl(a_{0})\big),\subdiv\big(\cyl(a_{1})\big),\cyl(d^{0}_{j}),s\big(\cyl(a_{1})\big) \circ \cyl\big(m^{\cylinder}_{j}\big),s\big(\cyl(a_{0})\big),\subdiv\big(\cyl(j)\big)} %
%
Earlier in the proof, we established that the following diagram in $\cl{A}$ commutes. \sq[{5,3}]{\cyl^{2}(a_{0}),\subdiv\big(\cyl(a_{0})\big),\cyl(a_{0}),\cyl(a_{1}),s\big(\cyl(a_{0})\big),u \circ \subdiv\big(\cyl(j)\big),p\big(\cyl(a_{0})\big),\cyl(j)} %
%
In addition, by definition of $\sigma$, the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),s\big(\cyl(a_{1})\big),u,\sigma} %
%
Putting the last three observations together, we have that the following diagram in $\cl{A}$ commutes, completing our proof of the claim. \sq[{4,3}]{\cyl^{2}(a_{0}),\cyl\big(a^{\cylinder}_{j}\big),\cyl(a_{0}),\cyl(a_{1}),\cyl(d^{0}_{j}),\sigma \circ \cyl\big(m^{\cylinder}_{j}\big),p\big(\cyl(a_{0})\big),\cyl(j)} %
%
Next, we claim that the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{\cyl(a_{1}),\cyl\big(a^{\cylinder}_{j}\big),a_{1},\cyl(a_{1}),\cyl(d^{1}_{j}),\sigma \circ \cyl\big(m^{\cylinder}_{j}\big),p(a_{1}),i_{0}(a_{1})} %
%
We have that the following diagram in $\cl{A}$ commutes. \tri{a_{1},a^{\cylinder}_{j},\cyl(a_{1}),d^{1}_{j},m^{\cylinder}_{j},i_{0}(a_{1})} %
%
Thus the following diagram in $\cl{A}$ commutes. \tri[{4,3}]{\cyl(a_{1}),\cyl\big(a^{\cylinder}_{j}\big),\cyl^{2}(a_{1}),\cyl(d^{1}_{j}),\cyl\big(m^{\cylinder}_{j}\big),\cyl\big(i_{0}(a_{1})\big)} %
%
The following diagram in $\cl{A}$ also commutes. \sq[{5,3}]{\cyl(a_{1}),\cyl^{2}(a_{1}),\subdiv(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl\big(i_{0}(a_{1})\big),s\big(\cyl(a_{1})\big),s(a_{1}),\subdiv\big(i_{0}(a_{1})\big)} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq[{4,3}]{\cyl(a_{1}),\cyl\big(a^{\cylinder}_{j}\big),a_{1},\cyl(a_{1}),\cyl(d^{1}_{j}),s\big(\cyl(a_{1})\big) \circ \cyl\big(m^{\cylinder}_{j}\big),s(a_{1}),\subdiv\big(i_{0}(a_{1})\big)} %
%
Earlier in the proof, we established that the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{1}),\subdiv(a_{1}),a_{1},\cyl(a_{1}),s(a_{1}),u \circ \subdiv\big(i_{0}(a_{1})\big),p(a_{1}),i_{0}(a_{1})} %
%
In addition, by definition of $\sigma$, the following diagram in $\cl{A}$ commutes. \tri[{5,3}]{\cyl^{2}(a_{1}),\subdiv\big(\cyl(a_{1})\big),\cyl(a_{1}),s\big(\cyl(a_{1})\big),u,\sigma} %
%
Putting the last three observations together, we have that the following diagram in $\cl{A}$ commutes, completing the proof of the claim. \sq[{4,3}]{\cyl(a_{1}),\cyl\big(a^{\cylinder}_{j}\big),a_{1},\cyl(a_{1}),\cyl(d^{1}_{j}),\sigma \circ \cyl\big(m^{\cylinder}_{j}\big),p(a_{1}),i_{0}(a_{1})} %
%
We have now shown that the following diagram in $\cl{A}$ commutes. \pushout[{5,3,-2}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl(a_{1}),\cyl(a^{\cylinder}_{j}),\cyl(a_{1}),\cyl\big(i_{0}(a_{0})\big),\cyl(d^{0}_{j}),\cyl(j),\cyl(d^{1}_{j}),\cyl(j) \circ p\big(\cyl(a_{0})\big),i_{0}(a_{1}) \circ p(a_{1}), \sigma \circ \cyl\big(m^{\cylinder}_{j}\big)} %
%
The following diagram in $\cl{A}$ commutes. \squareabovetriangle{\cyl^{2}(a_{0}),\cyl\big(a^{\cylinder}_{j}\big),\cyl(a_{0}),a^{\cylinder}_{j},\cyl(a_{1}),\cyl(d^{0}_{j}),p\big(a^{\cylinder}_{j}\big),p\big(\cyl(a_{0})\big),d^{0}_{j},m^{\cylinder}_{j},\cyl(j)} %
%
The following diagram in $\cl{A}$ also commutes. \squareabovetriangle{\cyl(a_{1}),\cyl\big(a^{\cylinder}_{j}\big),a_{1},a^{\cylinder}_{j},\cyl(a_{1}),\cyl(d^{1}_{j}),p\big(a^{\cylinder}_{j}\big),p(a_{1}),d^{1}_{j},m^{\cylinder}_{j},i_{0}(a_{1})} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \pushout[{5,3,-2}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl(a_{1}),\cyl(a^{\cylinder}_{j}),\cyl(a_{1}),\cyl\big(i_{0}(a_{0})\big),\cyl(d^{0}_{j}),\cyl(j),\cyl(d^{1}_{j}),\cyl(j) \circ p\big(\cyl(a_{0})\big),i_{0}(a_{1}) \circ p(a_{1}), m^{\cylinder}_{j} \circ p(a^{\cylinder}_{j})} %
%
Since $\cyl$ preserves mapping cylinders with respect to $\cylinder$, the following diagram in $\cl{A}$ is co-cartesian. \sq[{5,3}]{\cyl(a_{0}),\cyl^{2}(a_{0}),\cyl(a_{1}),\cyl(a^{\cylinder}_{j}),\cyl\big(i_{0}(a_{0})\big),\cyl(d^{0}_{j}),\cyl(j),\cyl(d^{1}_{j})} %
%
Appealing to the universal property of $\cyl\big(a^{\cylinder}_{j}\big)$, we deduce that the following diagram in $\cl{A}$ commutes, as required. \sq{\cyl(a^{\cylinder}_{j}),a^{\cylinder}_{j},\cyl^{2}(a_{1}),\cyl(a_{1}),p(a^{\cylinder}_{j}),m^{\cylinder}_{j},\cyl(m^{\cylinder}_{j}),\sigma} 

\end{proof}

\begin{prpn} \label{NormallyClovenFibrationsHaveRLPWithRespectToArrowsAdmittingAStrongDeformationRetractionProposition} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which admits a strong deformation retraction \ar{a_{1},a_{0},r} with respect to $\cylinder$, and let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which is a normally cloven fibration with respect to $\cylinder$. 

For any arrows \ar{a_{0},a_{2},g_{0}} and \ar{a_{1},a_{3},g_{1}} of $\cl{A}$ such that the diagram \sq{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$ commutes, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l}  \end{prpn}

\begin{proof} Since $r$ is a strong deformation retraction of $j$ with respect to $\cylinder$, there is a homotopy \ar{\cyl(a_{1}),a_{1},h} under $a_{0}$ from $jr$ to $id(a_{1})$ with respect to $\cylinder$. In particular, the following diagram in $\cl{A}$ commutes. \tri{a_{1},\cyl(a_{1}),a_{1},i_{0}(a_{1}),h, j \circ r} %
%
By assumption, we have that the following diagram in $\cl{A}$ commutes. \sq{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1}} %
%
Together, the commutativity of these two diagrams implies that the following diagram in $\cl{A}$ commutes. \sq{a_{1},a_{2},\cyl(a_{1}),a_{3},g_{0} \circ r,f, i_{0}(a_{1}), g_{1} \circ h} %
%
For any object $a$ of $\cl{A}$, let \ar{\Delta^{\cylinder}_{f,a},\Omega^{\cylinder}_{f,a},k_{a}} denote the map of the cleavage with which $f$ is equipped. %
%
Let \ar{\cyl(a_{1}),a_{2},k} denote the arrow $k_{a_{1}}(g_{0} \circ r, g_{1} \circ h)$ of $\cl{A}$. We have that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{1},a_{2},\cyl(a_{1}),a_{3},g_{0}\circ r,f,i_{0}(a_{1}),g_{1} \circ h,k} %
%
Let \ar{a_{1},a_{2},l} denote the arrow $k \circ i_{1}(a_{1})$ of $\cl{A}$. We claim that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} 

Firstly, we have that the following diagram in $\cl{A}$ commutes. \squareofthreetrianglestwo{a_{1},a_{2},\cyl(a_{1}),a_{1},a_{3},l,f,id,g_{1},i_{1}(a_{1}),k,h} Thus the triangle \tri{a_{1},a_{2},a_{3},l,f,g_{1}} in $\cl{A}$ commutes. It remains to prove the commutativity of the triangle \tri{a_{0},a_{1},a_{2},j,l,g_{0}} in $\cl{A}$. 

Let \ar{\cyl(a_{0}),a_{2},k'} denote the arrow $k_{a_{0}}\big(g_{0} \circ r \circ j, g_{1} \circ h \circ \cyl(j) \big)$ of $\cl{A}$. Since $f$ is a normally cloven fibration with respect to $\cylinder$, its cleavage satisfies property (ii) of Definition \ref{NormallyClovenFibrationCylinderDefinition}. Thus the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),\cyl(a_{1}),a_{3},\cyl(j),k,k'} %
%
By definition of $k'$, we have that the following diagram in $\cl{A}$ commutes. \liftingsquare[{7,3}]{a_{0},a_{2},\cyl(a_{0}),a_{3},g_{0} \circ r \circ j,f,i_{0}(a_{1}),g_{1} \circ h \circ \cyl(j),k'} %
%
By the commutativity of the diagram \tri{a_{0},a_{1},a_{0},j,r,id} in $\cl{A}$, we thus have that the following diagram in $\cl{A}$ commutes. \liftingsquare[{7,3}]{a_{0},a_{2},\cyl(a_{0}),a_{3},g_{0}, f, i_{0}(a_{0}), g_{1} \circ h \circ \cyl(j),k'} %
%
The following diagram in $\cl{A}$ also commutes, by definition of $h$. \sq{\cyl(a_{0}),a_{0},\cyl(a_{1}),a_{1},p(a_{0}),j,\cyl(j),h} %
%
Hence the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),a_{0},a_{3},p(a_{0}),g_{1} \circ j,g_{1} \circ h \circ \cyl(j)} %
%
We now have that the following diagram in $\cl{A}$ commutes. \liftingsquare[{7,3}]{a_{0},a_{2},\cyl(a_{0}),a_{3},g_{0}, f, i_{0}(a_{0}), g_{1} \circ j \circ p(a_{0}),k'} %
%
Since $f$ is a normally cloven fibration with respect to $\cylinder$, its cleavage satisfies property (i) of Definition \ref{NormallyClovenFibrationCylinderDefinition}. Thus the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),a_{0},a_{2},p(a_{0}),g_{0},k'} %
%
Putting everything together, we have that the following diagram in $\cl{A}$ commutes. \squarewithdiagonalthree{\cyl(a_{0}),\cyl(a_{1}),a_{0},a_{2},\cyl(j),k,p(a_{0}),g_{0},k'} %
%
Thus the following diagram in $\cl{A}$ commutes, as required. \trapeziumsfive[{3,3.5,0,0,0}]{a_{0},a_{1},\cyl(a_{0}),\cyl(a_{1}),a_{0},a_{2},j,l,id,g_{0},i_{1}(a_{0}),i_{1}(a_{1}),\cyl(j),p(a_{0}),k} 
\end{proof} 

\begin{cor} \label{NormallyClovenFibrationRLPTrivialCofibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and that $\cyl$ preserves mapping cylinders with respect to $\cylinder$. 

Let $\cocylinder = \big( \cocyl, e_0, e_1, c, v', \subdiv', r_{0}', r_{1}', s', \Gamma_{ul}'  \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v'$ compatible with $c$, a subdivision structure $\big( \subdiv', r_{0}', r_{1}', s' \big)$ compatible with $c$, and an upper left connection structure $\Gamma_{ul}'$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which is a trivial cofibration with respect to $\cylinder$, and let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which is a normally cloven fibration with respect to $\cocylinder$. 

For any commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1},l} \end{cor}

\begin{proof} By Corollary \ref{TrivialCofibrationAdmitsStrongDeformationRetractionCorollary}, we have that $j$ admits a strong deformation retraction with respect to $\cylinder$. By Proposition \ref{NormallyClovenFibrationCoCylinderIffNormallyClovenFibrationCylinderProposition}, we have that $f$ is a normally cloven fibration with respect to $\cylinder$. Thus we may appeal to Proposition \ref{NormallyClovenFibrationsHaveRLPWithRespectToArrowsAdmittingAStrongDeformationRetractionProposition} for a construction of $l$. \end{proof} 

\begin{cor} \label{TrivialFibrationRLPNormallyClovenCofibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{ul} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$, and an upper left connection structure $\Gamma_{ul}$. 

Let $\cocylinder = \big( \cocyl, e_0, e_1, c, v', \subdiv', r_{0}', r_{1}', s', \Gamma_{lr}', \Gamma_{ur}' \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v'$ compatible with $c$, a subdivision structure $\big( \subdiv', r_{0}', r_{1}', s' \big)$ compatible with $c$, a lower right connection structure $\Gamma_{lr}'$, and an upper right connection structure $\Gamma_{ur}'$. 

Suppose that $\Gamma_{lr}'$ and $\Gamma_{ur}'$ are compatible with the subdivision structure $\big( \subdiv', r_{0}', r_{1}', s' \big)$, and that $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$. %
%
Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which is a normally cloven cofibration with respect to $\cylinder$, and let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which is a trivial fibration with respect to $\cocylinder$. 

For any commutative diagram \sq{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{1},a_{2},a_{3},g_{0},f,j,g_{1},l} \end{cor}

\begin{proof} Follows immediately from Corollary \ref{NormallyClovenFibrationRLPTrivialCofibrationCorollary} by duality. \end{proof} 

\begin{cor} \label{CHEPHoldsCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{lr}, \Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. %
%
Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and that $\cyl$ preserves mapping cylinders with respect to $\cylinder$. 

Let $f$ be an arrow of $\cl{A}$ which is a normally cloven fibration with respect to $\cylinder$, and let $j$ be an arrow of $\cl{A}$ which is a cofibration with respect to $\cylinder$. Then $f$ has the covering homotopy extension property with respect to $j$ and $\cylinder$. \end{cor} 

\begin{proof} Follows immediately from Proposition \ref{RetractionIsStrongDeformationRetractionProposition} and Proposition \ref{NormallyClovenFibrationsHaveRLPWithRespectToArrowsAdmittingAStrongDeformationRetractionProposition}. \end{proof}

\begin{cor} \label{TrivialNormallyClovenFibrationRLPCofibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr}, \Gamma_{ur} \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, an involution structure $v$ compatible with $p$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $p$, an upper left connection structure $\Gamma_{ul}$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. %
%
Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and that $\cyl$ preserves mapping cylinders with respect to $\cylinder$. 

Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. %

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which is a cofibration with respect to $\cylinder$, and let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which is a trivial normally cloven fibration with respect to $\cocylinder$. 

For any commutative diagram \sq{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} \end{cor}

\begin{proof} By Proposition \ref{NormallyClovenFibrationCoCylinderIffNormallyClovenFibrationCylinderProposition}, we have that $f$ is a normally cloven fibration with respect to $\cylinder$. Thus by Corollary \ref{CHEPHoldsCorollary}, $f$ has the covering homotopy extension property with respect to $j$ and $\cylinder$. 

Moreover, by Corollary \ref{TrivialFibrationIsStrongDeformationRetractionCorollary}, there is an arrow \ar{a_{3},a_{2},j'} of $\cl{A}$ such that $f$ is a strong deformation retraction of $j'$ with respect to $\cocylinder$. Thus we may appeal to Proposition \ref{CHEPImpliesLiftingAxiomProposition} for a construction of $l$. \end{proof}

\begin{cor} \label{FibrationRLPTrivialNormallyClovenCofibrationCorollary} Let $\cylinder = \big( \cyl, i_{0}, i_{1}, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$. Let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c, v, \subdiv, r_{0}, r_{1}, s, \Gamma_{ul}, \Gamma_{lr}, \Gamma_{ur} \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$, an involution structure $v$ compatible with $c$, a subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$ compatible with $c$, an upper left connection structure $\Gamma_{ul}$, a lower right connection structure $\Gamma_{lr}$, and an upper right connection structure $\Gamma_{ur}$. 

Suppose that $\Gamma_{lr}$ and $\Gamma_{ur}$ are compatible with the subdivision structure $\big( \subdiv, r_{0}, r_{1}, s \big)$, and that $\cocyl$ preserves mapping co-cylinders with respect to $\cocylinder$. %
%
Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. %

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$ which is a trivial normally cloven cofibration with respect to $\cylinder$, and let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which is a fibration with respect to $\cocylinder$. 

For any commutative diagram \sq{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} \end{cor}

\begin{proof} Follows immediately from Corollary \ref{TrivialNormallyClovenFibrationRLPCofibrationCorollary} by duality. \end{proof} 

\end{chapter}
