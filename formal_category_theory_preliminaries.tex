\begin{chapter}{Formal category theory preliminaries} \label{FormalCategoryTheoryPreliminariesChapter}

In \ref{StructuresUponACylinderOrACoCylinderChapter}, we introduce various structures with which a cylinder or co-cylinder may be able to be equipped. The structures upon a co-cylinder are formally dual to those upon a cylinder. This duality, which is of a 2-categorical rather than a 1-categorical nature, as was already observed by Gray in \cite{GrayFibredAndCofibredCategories}, will manifest itself throughout this work. 

In order to express the duality we shall work throughout in a strict 2-category equipped with a strict final object. We think of the objects of this 2-category as formal categories. We now introduce these ideas as far as we shall need.

\begin{assum} Let $\cl{C}$ be a strict 2-category, and let $1$ be a final object of $\cl{C}$. \end{assum} 

\begin{defn} Let $\cl{A}$ be an object of $\cl{C}$. An {\em object} of $\cl{A}$ is a 1-arrow \ar{1,\cl{A}} of $\cl{C}$. \end{defn}

\begin{defn} Let $\cl{A}$ be an object of $\cl{C}$. Given objects $a_{0}$ and $a_{1}$ of $\cl{A}$, an {\em arrow} of $\cl{A}$ from $a_{0}$ to $a_{1}$ is  a 2-arrow \ar{a_{0},a_{1},} of $\cl{C}$. \end{defn}

\begin{rmk} Let $\underline{\mathsf{CAT}}$ denote the strict 2-category of large categories. The definitions above are motivated by the observation that, for any category $\cl{A}$, the category $\underline{\mathsf{Hom}}_{\underline{\mathsf{CAT}}}(1,\cl{A})$ is isomorphic to $\cl{A}$.  \end{rmk}

\begin{defn} Let $\cl{A}$ be an object of $\cl{C}$, and let \ar{a_{0},a_{1},f_{0}} and \ar{a_{1},a_{2},f_{1}} be arrows of $\cl{A}$. The {\em composition} $f_{1} \circ f_{0}$ of $f_{1}$ and $f_{0}$ in $\cl{A}$ is their composition in $\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A})$. \end{defn}

\begin{defn} A {\em diagram} in $\cl{A}$ is a diagram in $\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A})$. We define a {\em commutative diagram} in $\cl{A}$, a {\em co-cartesian square} in $\cl{A}$, and a {\em cartesian square} in $\cl{A}$ in the same way. \end{defn}

\begin{notn} Given 1-arrows 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cl{A}_{0} \& |(1-0)| \cl{A}_{1} \& |(2-0)| \cl{A}_{2} \\ 
};
	
\draw[->] ({0-0}.north east) to node[auto] {$F_{0}$} ({1-0}.north west);
\draw[->] ({0-0}.south east) to node[auto,swap] {$F_{1}$} ({1-0}.south west);
\draw[->] (1-0) to node[auto] {$G$} (2-0); 

\end{tikzpicture} 

\end{diagram} %
%
of $\cl{C}$, and a 2-arrow \ar{F_{0},F_{1},\eta} of $\cl{C}$, we denote by $G \cdot \eta$ the 2-arrow \ar{GF_{0},GF_{1},} of $\cl{C}$ obtained by horizontal compositition of $id(G)$ and $\eta$. It is sometimes referred to as the {\em whiskering} of $G$ and $\eta$.

Similarly, given 1-arrows

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cl{A}_{0} \& |(1-0)| \cl{A}_{1} \& |(2-0)| \cl{A}_{2} \\ 
};
	
\draw[->] ({1-0}.north east) to node[auto] {$F_{0}$} ({2-0}.north west);
\draw[->] ({1-0}.south east) to node[auto,swap] {$F_{1}$} ({2-0}.south west);
\draw[->] (0-0) to node[auto] {$G$} (1-0); 

\end{tikzpicture} 

\end{diagram} %
%
of $\cl{C}$, and a 2-arrow \ar{F_{0},F_{1},\eta} of $\cl{C}$, we denote by $\eta \cdot G$ the 2-arrow \ar{F_{0}G,F_{1}G,} of $\cl{C}$ obtained by horizontal composition of $\eta$ and $id(G)$. It is also sometimes referred to as the {\em whiskering} of $\eta$ and $G$. \end{notn}

\begin{notn} Let \ar{\cl{A}_{0},\cl{A}_{1},F} be a 1-arrow of $\cl{C}$. If $a$ is an object of $\cl{A}_{0}$, we denote by $F(a)$ the object of $\cl{A}_{1}$ defined by the 1-arrow $F \circ a$ of $\cl{C}$. 

If \ar{a_{0},a_{1},f} is an arrow of $\cl{A}_{0}$, we denote by $F(f)$ the arrow of $\cl{A}_{1}$ from $F(a_{0})$ to $F(a_{1})$ defined by the whiskered 2-arrow $F \cdot f$ of $\cl{C}$. \end{notn}

\begin{rmk} \label{1ArrowsAsFunctors} In this way, we think of a 1-arrow \ar{\cl{A}_{0},\cl{A}_{1},F} of $\cl{C}$ as a functor from $\cl{A}_{0}$ to $\cl{A}_{1}$, corresponding to the functor \ar[6]{{\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A}_{0})},{\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A}_{1}).},{\underline{\mathsf{Hom}}_{\cl{C}}(1,F)}} \end{rmk}

\begin{notn} \label{2ArrowAsNaturalTransformationNotation} Let \pair{\cl{A}_{0},\cl{A}_{1},F_{0},F_{1}} be 1-arrows of $\cl{C}$, and let \ar{F_{0},F_{1},\eta} be a 2-arrow of $\cl{C}$. If $a$ is an object of $\cl{A}_{0}$, we denote by $\eta(a)$ the arrow of $\cl{A}_{1}$ from $F_{0}(a)$ to $F_{1}(a)$ defined by the whiskered 2-arrow $\eta \cdot a$ of $\cl{C}$. \end{notn}

\begin{rmk} Let \pair{\cl{A}_{0},\cl{A}_{1},F_{0},F_{1}} be 1-arrows of $\cl{C}$, thought of as functors from $\cl{A}_{0}$ to $\cl{A}_{1}$, as in Remark \ref{1ArrowsAsFunctors}. We think of a 2-arrow \ar{F_{0},F_{1},\eta} of $\cl{C}$ as a natural transformation from $F_{0}$ to $F_{1}$, corresponding to the natural transformation \ar[6]{{\underline{\mathsf{Hom}}_{\cl{C}}(1,F_{0})},{{\underline{\mathsf{Hom}}_{\cl{C}}(1,F_{1})}.},{\underline{\mathsf{Hom}}_{\cl{C}}(1,\eta)}} \end{rmk}

\begin{notn} We denote by $\cl{C}^{op}$ the 2-category obtained from $\cl{C}$ by reversing all 2-arrows. If $f$ is a 2-arrow of $\cl{C}$, we denote by $f^{op}$ the corresponding 2-arrow of $\cl{C}^{op}$. When viewing an object $\cl{A}$ of $\cl{C}$ as an object of $\cl{C}^{op}$, we denote it by $\cl{A}^{op}$. Thus $\underline{\mathsf{Hom}}_{\cl{C}^{op}}(1,\cl{A}^{op})$ is the opposite category of $\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A})$. In particular, if \ar{a_{0},a_{1},f} defines an arrow of $\cl{A}$, then the 2-arrow $f^{op}$ of $\cl{C}^{op}$ defines an arrow of $\cl{A}^{op}$ from $a_{1}$ to $a_{0}$. \end{notn}

\begin{recollection} \label{AdjunctionRecollection} An {\em adjunction} between a pair $(F,G)$ of 1-arrows \adjunction{\cl{A}_{0},\cl{A}_{1},F,G} of $\cl{C}$ is a 2-arrow \ar{id(\cl{A}_{0}),GF,\eta} of $\cl{C}$, and a 2-arrow \ar{FG,id(\cl{A}_{1}),\zeta} of $\cl{C}$, such that the diagram \tri{F,FGF,F,F \eta, \zeta F, id} in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A}_{0},\cl{A}_{1})$ commutes, and such that the diagram \tri{G,GFG,G,\eta G, G \zeta,id} in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A}_{1},\cl{A}_{0})$ commutes. We refer to $F$ as a {\em left adjoint} of $G$, and to $G$ as a {\em right adjoint} of $F$. 

If we have an adjunction between $F$ and $G$, then for any object $\cl{A}$ of $\cl{C}$, the natural transformations $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\eta)$ and $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\zeta)$ define an adjunction between the following pair of functors. \adjunction[7]{{\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A}_{0})},{\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},\cl{A}_{1})},{\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},F)},{\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A},G)}} %
%
In particular, we have a natural isomorphism \ar{{\mathsf{Hom}_{\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A}_{1})}\Big(\underline{\mathsf{Hom}}_{\cl{C}}\big(1,F(-)\big),-\Big)},{\mathsf{Hom}_{\underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A}_{0})}\Big(-,\underline{\mathsf{Hom}}_{\cl{C}}\big(1,G(-)\big)\Big)},\mathsf{adj}} of functors \ar{{\big( \mathsf{Hom}_{\cl{C}}(1,\cl{A}_{0}) \big)^{op} \times \mathsf{Hom}_{\cl{C}}(1,\cl{A}_{1})},{\mathsf{Set}.}} %
%
Adopting the shorthand 

\begin{diagram}

\begin{tikzpicture} [>=stealth, column 1/.style={anchor=east}, column 2/.style={anchor=west}]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=3 em, row sep=1em, nodes={anchor=center}]
{
|(0-0)| \cl{A} \& |(1-0)| \underline{\mathsf{Hom}}_{\cl{C}}(1,\cl{A}), \\  
|(0-1)| F   \& |(1-1)| \underline{\mathsf{Hom}}_{\cl{C}}\big(1,F \big), \\ 
|(0-2)| G   \& |(1-2)| \underline{\mathsf{Hom}}_{\cl{C}}\big(1,G \big), \\
};

\draw[<->] (0-0) to (1-0);
\draw[<->] (0-1) to (1-1);
\draw[<->] (0-2) to (1-2);

\end{tikzpicture}

\end{diagram} %
%
we shall write the above natural isomorphism as \ar{{\mathsf{Hom}_{\cl{A}_{1}}\big(F(-),-\big)},{\mathsf{Hom}_{\cl{A}_{0}}\big(-,G(-)\big).},\mathsf{adj}} \end{recollection}

\begin{defn} \label{PushoutsPullbacks2ArrowsArePushoutsPullbacksInFormalCategoriesTerminology} If for any objects $\cl{A}_{0}$ and $\cl{A}_{1}$ of $\cl{C}$, any object $a$ of $\cl{A}_{0}$, and any co-cartesian (respectively cartesian) square \sq{F_{0},F_{1},F_{2},F_{3},\eta_{0},\eta_{1},\eta_{2},\eta_{3}} in $\underline{\mathsf{Hom}}_{\cl{C}}(\cl{A}_{0},\cl{A}_{1})$, the square \sq{F_{0}(a),F_{1}(a),F_{2}(a),F_{3}(a),\eta_{0}(a),\eta_{1}(a),\eta_{2}(a),\eta_{3}(a)} in $\cl{A}_{1}$ is co-cartesian (respectively cartesian), we write that {\em pushouts (respectively pullbacks) of 2-arrows of $\cl{C}$ give rise to pushouts (respectively pullbacks) in formal categories}. \end{defn}

\begin{rmk} In both $\underline{\mathsf{CAT}}$ and $\underline{\mathsf{CAT}}^{op}$, pushouts and pullbacks of 2-arrows give rise to pushouts and pullbacks in formal categories, more or less by definition. The author expects that colimits and limits of 2-arrows equally give rise to colimits and limits in formal categories for quite general 2-categories, for instance 2-topoi. \end{rmk}
 
\begin{rmk} It would certainly be possible for us to work with weak rather than strict 2-categories. However, we are motivated by ordinary categories. In addition to $\underline{\mathsf{CAT}}$, the only 2-category of importance to us is $\underline{\mathsf{CAT}}^{op}$, exactly in order to capture duality. Thus it is sufficient for us to work with strict 2-categories, and we do so in order to avoid the distraction of coherency. \end{rmk}

\end{chapter}
