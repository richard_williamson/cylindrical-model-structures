\begin{chapter}{Introduction} \label{IntroductionChapter}

Model structures are cherished --- powerful, but hard to construct. In this work we build a model structure from the simple point of departure of a structured interval in a monoidal category --- more generally, a structured cylinder and a structured co-cylinder in a category.

\section*{Abstract homotopy theory} \label{PlaceWithinAbstractHomotopyTheorySubsection} \label{PlaceWithinAbstractHomotopySubsection} The first steps towards an abstract homotopy theory were, for us, taken in the early 1950s by Kan --- though there is a considerable prehistory, for example in the work of Whitehead. Kan isolated in \cite{KanAbstractHomotopyTheoryII} the notion of a cylinder in a category and of homotopy with respect to a cylinder. 

Kan's immediate interest lay in the categories of cubical and simplicial sets. Whilst both categories admit a cylinder, the corresponding homotopies cannot be composed or reversed. What are now known as Kan complexes were introduced as a remedy. 

Abstract homotopy theory has subsequently evolved in two branches, which have been explored rather independently. The first is the theory of model categories and its many variants and weakenings. The second is much less known. Its origins lie in the observation that the cylinder in topological spaces admits a much richer structure than the cylinder of the categories of simplicial or cubical sets. Two directions have been followed in capturing this richer structure of the topological cylinder in an abstract setting. 

\begin{figure}[h] \label{FigureAbstractHomotopyTheory}

\centering

\begin{tikzpicture} [>=stealth]

\node[align=center,text width=2cm,anchor=north] (kan) at (4,0) {Kan \\[0.5em]  1956 \\[0.5em] Cylinder in a category};

\node[align=center,text width=3cm,anchor=north,inner ysep=1em] (quillen) at (0,-5) {Quillen \\[0.5em]  1967 \\[0.5em] Model categories};

\node[align=center,text width=2cm,anchor=north,inner ysep=1em] (grandis) at (5,-5) {Grandis \\[0.5em]  1990s \\[0.5em] Structured cylinders};

\node[align=center,text width=2.5cm,anchor=north,inner ysep=1em] (kamps) at (9,-5) {Kamps \\[0.5em]  Late 1960s \\[0.5em] Kan conditions};

\draw[bend right=45] (kan.west) to (quillen.north);
\draw[bend left=45] (kan.east) to (7,-3);
\draw[bend right=45] (7,-3) to (grandis.north);
\draw[bend left=45] (7,-3) to (kamps.north);
\draw[<-,dashed] (quillen.east) to node[auto] {our work} ++ (2.1,0);
\end{tikzpicture}

\caption*{Figure: Approaches to abstract homotopy theory}

\end{figure}

The first, begun by Kamps in the late 1960s in works such as \cite{KampsKanBedingungenUndAbstrakteHomotopietheorie}, explores the homotopy theory with respect to a cylinder whose associated cubical set satisfies properties similar to those of a Kan complex. This is a global approach, with the axioms requiring consideration of all arrows of a category. 

The second emerges out of works of Brown, Higgins, and others on cubical sets with connections, for example the paper \cite{BrownHigginsOnTheAlgebraOfCubes}. It is of a structural and categorical nature, involving a rainbow of natural transformations intertwining a cylinder and its corresponding double cylinder. This approach has been explored by Grandis in works such as \cite{GrandisCategoricallyAlgebraicFoundationsForHomotopicalAlgebra}. 

The book \cite{KampsPorterAbstractHomotopyAndSimpleHomotopyTheory} of Kamps and Porter gives a nice overview of both directions, with an emphasis on the first.

\section*{Outline}

The present work builds a bridge to model categories from the categorical approach to homotopy theory via structured intervals, cylinders, and co-cylinders. The theorems towards which all of our work leads are to be found in \ref{ModelStructureChapter}. Given a richly structured cylinder and co-cylinder in a category satisfying a certain strictness hypothesis, we prove that homotopy equivalences, cofibrations, and fibrations --- defined from an abstract point of view in the same way as for topological spaces --- equip this category with a model structure. 

More precisely, our theory typically gives rise to not one, but two model structures. We introduce in \ref{CofibrationsAndFibrationsChapter} a notion of a normally cloven cofibration, and of a normally cloven fibration. One of our model structures is defined by homotopy equivalences, cofibrations, and normally cloven fibrations, whilst the other is defined by homotopy equivalences, normally cloven cofibrations, and fibrations. 

The structures on a cylinder and a co-cylinder with which we work are defined in \ref{StructuresUponACylinderOrACoCylinderChapter}, along with our strictness hypothesis. Often, in practise, we construct a structured cylinder and a structured co-cylinder by means of a structured interval in a monoidal category. This is discussed in \ref{StructuresUponAnIntervalChapter}. 

We work in a 2-categorical setting, introduced in \ref{FormalCategoryTheoryPreliminariesChapter}, which allows us to express a duality between homotopy theory with respect to a cylinder on the one hand, and homotopy theory with respect to a co-cylinder on the other. This duality manifests itself throughout. 

In \ref{CylindricalAdjunctionsChapter}, we discuss a notion of adjunction between a cylinder and a co-cylinder, in the presence of which the corresponding homotopy theories coincide. For the remainder of this outline, we shall indicate neither the particular structures involved at different points, nor whether we are working with a cylinder, a co-cylinder, or both. These matters are carefully treated in the remainder of the work.

In \ref{HomotopyAndRelativeHomotopyChapter}, we define homotopies and relative homotopies with respect to a cylinder or co-cylinder. We demonstrate that we can compose and reverse them.

If our strictness hypothesis holds, we prove in \ref{MappingCylindersAndMappingCoCylindersChapter} that the mapping cylinder of a map gives rise to a factorisation of it into a normally cloven cofibration followed by a strong deformation retraction, and that the mapping co-cylinder of a map gives rise to a factorisation of it into a section of strong deformation retraction followed by a normally cloven fibration.

In \ref{DoldTheoremChapter}, we characterise trivial fibrations as strong deformation retractions, and characterise trivial cofibrations as sections of strong deformation retractions. This is by means of an abstraction of Dold's theorem for topological spaces, on homotopy equivalences under or over an object. 

Assuming once more that our strictness hypothesis holds, we prove in \ref{FactorisationAxiomsChapter} that a trivial cofibration is exactly a section of a strong deformation retraction, and dually that a trivial fibration is exactly a strong deformation retraction. With our mapping cylinder and mapping co-cylinder factorisations to hand, we deduce that the factorisation axioms for a model structure hold.

In \ref{LiftingAxiomsChapter}, we prove that the canonical map from the mapping cylinder of a map to the cylinder at its target admits a strong deformation retraction. We prove that normally cloven fibrations have the right lifting property with respect to sections of strong deformation retractions. We deduce that normally cloven fibrations have the covering homotopy extension property --- introduced in \ref{CoveringHomotopyExtensionPropertyChapter} --- with respect to cofibrations. 

This allows us to prove that cofibrations have the left lifting property with respect to trivial normally cloven fibrations. We conclude that the lifting axioms hold for one of our model structures. The lifting axioms for the other model structure follow by duality.  

\section*{Folk model structure} \label{ExamplesFutureDirectionsSubsection} This work was conceived as a step towards the construction of a model category of $n$-groupoids satisfying, in a strong sense, the homotopy hypothesis. The aim was to construct the folk model structure on categories and groupoids in a way which we could generalise to $n$-groupoids.

We demonstrate in \ref{ExampleChapter} that our work indeed gives a new construction of the folk model structure on categories and groupoids. The construction of a model category of $n$-groupoids by means of the present work is the point of departure of joint work with Marius Thaule which is in preparation.

\section*{Further examples} Our work gives rise to many other model structures. We discuss three. 

\begin{itemize}[itemsep=1em,topsep=1em]

\item[(1)] Let $\mathsf{Ch}(\cl{A})$ denote the category of chain complexes in an additive category $\cl{A}$ with finite limits and colimits. The cylinder and co-cylinder functors \ar{\mathsf{Ch}(\cl{A}),\mathsf{Ch}(\cl{A})} of homological algebra can be equipped with all the structures of \ref{StructuresUponACylinderOrACoCylinderChapter}. We refer the reader to \S{4.4.2} of the book \cite{GrandisDirectedAlgebraicTopology} of Grandis, for example. Our strictness hypothesis is satisfied. 

Our work thus gives a model structure on $\mathsf{Ch}(\cl{A})$ whose weak equivalences are chain homotopy equivalences. This model structure was constructed in a quite different way by Golasi{\'n}ski and Gromadzki in \cite{GolasinskiGromadzkiTheHomotopyCategoryOfChainComplexesIsAChainComplex}, appealing to a characterisation due to Kamps in \cite{KampsNoteOnNormalSequencesOfChainComplexes} of the fibrations and cofibrations.

\item[(2)] Let $\mathsf{Kan}_{\Delta}$ denote the category of algebraic Kan complexes introduced by Nikolaus in \cite{NikolausAlgebraicModelsForHigherCategories}. The objects of $\mathsf{Kan}_{\Delta}$ are Kan complexes with a chosen filling for every horn. The arrows of $\mathsf{Kan}_{\Delta}$ are morphisms of simplicial sets which respect the chosen horn fillings.

Our work gives a model structure on $\mathsf{Kan}_{\Delta}$, which we think of as akin to the model structure on topological spaces constructed by Str{\o}m in \cite{StromTheHomotopyCategoryIsAHomotopyCategory}. A different model structure on $\mathsf{Kan}_{\Delta}$ was constructed by Nikolaus in \cite{NikolausAlgebraicModelsForHigherCategories}, which we think of as akin to the Serre model structure on topological spaces.

The author conjectures that the identity functor defines a Quillen equivalence between these two model structures on $\mathsf{Kan}_{\Delta}$.

\item[(3)] Let $\mathsf{Top}$ denote the category of all topological spaces. The unit interval is exponentiable with respect to the cartesian monoidal structure on $\mathsf{Top}$ and can be equipped with all of the structures of \ref{StructuresUponAnIntervalChapter}. 

Our strictness hypothesis does not however hold.  Thus our work does not immediately give rise to a model structure on $\mathsf{Top}$. 

Nevertheless, homotopy equivalences in $\mathsf{Top}$ can be understood as homotopy equivalences with respect to the Moore co-cylinder, which to a topological space $X$ associates the set of pairs $(t,f)$ of a real number $t \in [0,\infty)$ and a map \ar{{[0,\infty)},X,f} such that $f(x) =t$ for all $x \geq t$, where this set is equipped with the subspace topology with respect to $[0,\infty) \times X^{[0,\infty)}$. Our strictness hypothesis does hold for the Moore co-cylinder.

The Moore co-cylinder does not however admit connection structures. There are two ways to get around this. Firstly, it is possible to generalise our work slightly to double cylinders and double co-cylinders which are not necessarily obtained by applying the cylinder or co-cylinder functor twice. We can then take our Moore double co-cylinder to consist of Moore rectangles as considered in the paper \cite{BrownMooreHyperrectanglesOnASpaceFormAStrictCubicalOmegaCategory} of Brown. Secondly, it should be possible to replace the connection structures in our work by the `strengths' of the paper \cite{VanDenBergGarnerTopologicalAndSimplicialModelsOfIdentityTypes} of van den Berg and Garner.

Following either route, we can obtain the model structure on $\mathsf{Top}$ constructed by Str{\o}m in \cite{StromTheHomotopyCategoryIsAHomotopyCategory} by working with respect to both the Moore co-cylinder and the usual cylinder and co-cylinder in $\mathsf{Top}$.

Around Easter 2012, Tobias Barthel and Bill Richter suggested to the author a construction of the Moore co-cylinder which could be carried out in a quite general setting. Thus our side-stepping of the failure of the strictness hypothesis to hold in $\mathsf{Top}$ may be able to be carried out more widely.   

The significance of the Moore co-cylinder for the construction of the Str{\o}m model structure on $\mathsf{Top}$ is explored in the paper \cite{BarthelRiehlOnTheConstructionOfFunctorialFactorizationsForModelCategories} of Barthel and Riehl.

\end{itemize}

\section*{Acknowledgements} I thank Kari, the wonderful funny bee to whom this work is dedicated, for everything. It is impossible to express how much your lifting of my spirit --- cakes of course playing a crucial role! --- has contributed to this work.

I thank Mum and Dad for everything over the years. I cannot express my gratitude. I would specifically like to thank you for your unwavering encouragement and belief throughout the writing of this work --- I am very lucky to have been able to phone up babbling incomprehensibly about mathematics!

I have the grandest pair of brothers in the world, Chris and Andrew. I thank you both for all the many laughter filled times --- for always being there for me.

I thank Babcia, Dziadziu, Grandma, and, beyond the grave, Grandad for everything they have done for me. I feel deeply that this work owes a great deal to your hard work throughout your lives.

To Alex, Tom, Richy, Shaun, Sian, Phil, and Katherine --- I count myself incredibly lucky to have got to know you all, and that we have such a close knit friendship. You all bring great joy to my life!

To Merav, Daniel, Nimrod, Henning, and Anne -- wonderful dinners, black and white films, and the fun brought into our lives by little Nimrod gave Kari and me great happiness in our time in Oxford, and I shall always look back with an ineffable fondness. I thank Henning for the very kind help with the final printing of the thesis, and for handing it over to the Bodleian library --- much appreciated!

I thank Rapha{\"e}l Rouquier greatly for his supervision over my four years in Oxford. That every turn in my path was greeted with enthusiasm, wisdom, and sincerity was a great encouragement to me.

I thank Tobias Barthel for many enjoyable discussions. I treasure our correspondence, and our sharing of mathematical dreams!   

I thank Nicholas Cooney for many pleasant conversations over coffee and in the pub, and for the great help with the submission of this work as my thesis! 

I thank Ulrike Tillmann for her advice and understanding at a crucial point in my time as a graduate student, and for her continuing encouragement.

I thank Michael Collins for his support and encouragement in the months before and during my time at Oxford, and for his insight in suggesting Rapha{\"e}l as a supervisor.  

To all at Oxenford Cricket Club --- especially my 2nd XI teammates --- for three years of immensely enjoyable summer Saturdays.

I thank Tim Porter and Kobi Kremnizer for their examination of my thesis, for their encouragement, and for their very helpful comments and suggestions. 

I thank Nils Baas greatly for his kindness in inviting us and helping us to settle down in Trondheim. I thank Nils and Andrew Stacey for their support in my finding a position at NTNU.

I thank Bj{\o}rn Ian Dundas for arranging the funding of my employment at NTNU as a guest researcher from January to March 2012. 

Finally to all our friends and family in Sandefjord, T{\o}nsberg, Larvik, and Trondheim for their help, well wishes, and understanding since we moved to Norway in August 2011 --- and of course for the dinners, waffles, and cakes\ldots a recurring theme!

\end{chapter}
