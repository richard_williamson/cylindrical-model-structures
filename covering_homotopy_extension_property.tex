\begin{chapter}{Covering homotopy extension property} \label{CoveringHomotopyExtensionPropertyChapter}

We introduce the covering homotopy extension property with respect to a cylinder $\cylinder$ in a formal category $\cl{A}$, and to an arrow $j$ of $\cl{A}$. 

Suppose that $\cylinder$ is equipped with a contraction structure $p$. Let $\cocylinder$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. If an arrow $f$ of $\cl{A}$ has the covering homotopy extension property with respect to $\cylinder$ and $j$, and if $f$ is, moreover, a strong deformation retraction with respect to $\cocylinder$, we prove that $f$ has the right lifting property with respect to $j$. This will be vital to us in \ref{LiftingAxiomsChapter}.

That the covering homotopy extension property could imply a right lifting property goes back to the proof of Theorem 9 in the paper \cite{StromNoteOnCofibrationsII} of Str{\o}m, and was explored in \S{6} of the paper \cite{HastingsFibrationsOfCompactlyGeneratedSpaces} of Hastings. Our proof is abstracted from these two papers. Already in \cite{HastingsFibrationsOfCompactlyGeneratedSpaces}, Hastings observed that an abstraction of this kind should be possible. A proof in an abstract setting is also given towards the end of \S{3} of Chapter II of the book \cite{KampsPorterAbstractHomotopyAndSimpleHomotopyTheory} of Kamps and Porter.  

In the category of topological spaces, Str{\o}m proved as Theorem 4 of \cite{StromNoteOnCofibrations} that fibrations have the covering homotopy extension property with respect to closed cofibrations. In Theorem 2.1 of \cite{HastingsFibrationsOfCompactlyGeneratedSpaces}, Hastings proved that in the category of compactly generated Hausdorff spaces, fibrations have the covering homotopy extension property with respect to arbitrary cofibrations. Related theorems go back to around 1960, if not further.

More recently, fibrations in the category of topological spaces satisfying the covering homotopy extension property with respect to cofibrations were investigated in the paper \cite{SchwanzelVogtStrongCofibrationsAndFibrationsInEnrichedCategories} of Schw{\"a}nzel and Vogt, in which they are referred to as strong fibrations. They are also discussed in Chapter 4 of the book \cite{MaySigurdssonParametrizedHomotopyTheory} of May and Sigurdsson, in which the same terminology is adopted. 

\begin{assum} Let $\cl{C}$ be a 2-category with a final object. Suppose that pushouts and pullbacks of 2-arrows of $\cl{C}$ give rise to pushouts and pullbacks in formal categories, in the sense of Definition \ref{PushoutsPullbacks2ArrowsArePushoutsPullbacksInFormalCategoriesTerminology}. Let $\cl{A}$ be an object of $\cl{C}$. As before, we view $\cl{A}$ as a formal category, writing of objects and arrows of $\cl{A}$. \end{assum} 

\begin{defn} Let $\cylinder = \big( \cyl, i_0, i_1 \big)$ be a cylinder in $\cl{A}$. Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$, and suppose that $\big( a^{\cylinder}_{j}, d^{0}_{j}, d^{1}_{j} \big)$ defines a mapping cylinder of $j$ with respect to $\cylinder$. 

An arrow \ar{a_{2},a_{3},f} of $\cl{A}$ has the {\em covering homotopy extension property} with respect to $j$ and $\cylinder$ if, for any commutative diagram \sq{a^{\cylinder}_{j},a_{2},\cyl(a_{1}),a_{3},g,f,m^{\cylinder}_{j},h} in $\cl{A}$, there is an arrow \ar{\cyl(a_{1}),a_{2},l} of $\cl{A}$, such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a^{\cylinder}_{j},a_{2},\cyl(a_{1}),a_{3},g,f,m^{\cylinder}_{j},h,l} \end{defn}

\begin{rmk} This definition is equivalent to that given towards the end of \S{3} of Chapter II of the book \cite{KampsPorterAbstractHomotopyAndSimpleHomotopyTheory} of Kamps and Porter. We shall not need this. \end{rmk}

\begin{prpn} \label{CHEPImpliesLiftingAxiomProposition} Let $\cylinder = \big( \cyl, i_0, i_1, p \big)$ be a cylinder in $\cl{A}$ equipped with a contraction structure $p$, and let $\cocylinder = \big( \cocyl, e_{0}, e_{1}, c \big)$ be a co-cylinder in $\cl{A}$ equipped with a contraction structure $c$. Suppose that $\cylinder$ is left adjoint to $\cocylinder$, and that the adjunction between $\cyl$ and $\cocyl$ is compatible with $p$ and $c$. 

Let \ar{a_{0},a_{1},j} be an arrow of $\cl{A}$, and suppose that $\big( a^{\cylinder}_{j}, d^{0}_{j}, d^{1}_{j} \big)$ defines a mapping cylinder of $j$ with respect to $\cylinder$. Let \ar{a_{2},a_{3},f} be an arrow of $\cl{A}$ which has the covering homotopy extension property with respect to $j$ and $\cylinder$. Moreover, let \ar{a_{3},a_{2},j'} be an arrow of $\cl{A}$ and suppose that $f$ is a strong deformation retraction of $j'$ with respect to $\cocylinder$. 

Then, for any commutative diagram \sq{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1}} in $\cl{A}$, there is an arrow \ar{a_{1},a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},l} \end{prpn} 

\begin{proof} Since $f$ is a strong deformation retraction of $j'$ with respect to $\cocylinder$, we have by Proposition \ref{StrongDeformationRetractionCoCylinderCharacterisationProposition} that there is a homotopy \ar{\cyl(a_{2}),a_{2},h} over $a_{3}$ from $j'f$ to $id(a_{2})$ with respect to $\cylinder$ and $(f,f)$. The following diagram in $\cl{A}$ commutes. \trapeziumstwo[{3,3,1,0}]{a_{0},\cyl(a_{0}),a_{1},a_{2},\cyl(a_{2}),a_{3},a_{2},i_{0}(a_{0}),\cyl(g_{0}),h,j,g_{1},j',g_{0},i_{0}(a_{2}),f} %
%
Thus there is a canonical arrow \ar{a^{\cyl}_{j},a_{2},u} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \pushout{a_{0},\cyl(a_{0}),a_{1},a^{\cylinder}_{j},a_{2},i_{0}(a_{0}),d^{0}_{j},j,d^{1}_{j},h \circ \cyl(g_{0}),j' \circ g_{1},u} %
%
By definition of $h$ as a homotopy over $a_{3}$ with respect to $\cylinder$ and $(f,f)$, the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{2}),a_{2},\cyl(a_{3}),a_{3},h,f,\cyl(f),p(a_{3})} Moreover, the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{2}),\cyl(a_{3}),a_{2},a_{3},\cyl(f),p(a_{3}),p(a_{2}),f} Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \sq{\cyl(a_{2}),a_{2},a_{2},a_{3},h,f,p(a_{2}),f} %
%
We now have that the following diagram in $\cl{A}$ commutes. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=3 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl(a_{0}) \&                     \& |(2-0)| a^{\cylinder}_{j} \\ 
                    \& |(1-1)| \cyl(a_{2}) \& |(2-1)| a_{2}        \\
                    \& |(1-2)| a_{2}       \&                     \\
|(0-3)| a_{0}       \& |(1-3)| a_{1}       \& |(2-3)| a_{3}       \\
};
	
\draw[->] (0-0) to node[auto] {$d^{0}_{j}$} (2-0);
\draw[->] (0-0) to node[auto,swap] {$p(a_{0})$} (0-3);
\draw[->] (0-0) to node[auto] {$\cyl(g_{0})$} (1-1);
\draw[->] (1-1) to node[auto] {$h$} (2-1);
\draw[->] (1-1) to node[auto,swap] {$p(a_{2})$} (1-2);
\draw[->] (1-2) to node[auto] {$f$} (2-3);
\draw[->] (0-3) to node[auto] {$g_{0}$} (1-2);
\draw[->] (2-0) to node[auto] {$u$} (2-1);
\draw[->] (2-1) to node[auto] {$f$} (2-3);
\draw[->] (0-3) to node[auto,swap] {$j$} (1-3);
\draw[->] (1-3) to node[auto,swap] {$g_{1}$} (2-3);
\end{tikzpicture} 

\end{diagram} %
%
The following diagram in $\cl{A}$ also commutes. \squareabovetriangle{a_{1},a^{\cylinder}_{j},a_{3},a_{2},a_{3},d^{1}_{j},u,g_{1},j',f,id}%
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \pushout[{4,3,0}]{a_{0},\cyl(a_{0}),a_{1},a^{\cylinder}_{j},a_{3},i_{0}(a_{0}),d^{0}_{j},j,d^{1}_{j},g_{1} \circ j \circ p(a_{0}), g_{1}, f \circ u} %
%
The following diagram in $\cl{A}$ also commutes.  

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=6 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| \cyl(a_{0}) \& |(1-0)| a^{\cylinder}_{j} \\ 
                    \& |(1-1)| \cyl(a_{1})   \\
|(0-2)| a_{0}       \& |(1-2)| a_{1}        \\
};
	
\draw[->] (0-0) to node[auto] {$d^{0}_{j}$} (1-0);
\draw[->] (0-0) to node[auto,swap] {$p(a_{0})$} (0-2);
\draw[->] (1-0) to node[auto] {$m^{\cylinder}_{j}$} (1-1);
\draw[->] (0-0) to node[auto,swap] {$\cyl(j)$} (1-1);
\draw[->] (1-1) to node[auto] {$p(a_{1})$} (1-2);
\draw[->] (0-2) to node[auto,swap] {$j$} (1-2);
\end{tikzpicture} 

\end{diagram} %
%
Hence the following diagram in $\cl{A}$ commutes. \tri{\cyl(a_{0}),a^{\cylinder}_{j},a_{3},d^{0}_{j},g_{1} \circ p(a_{1}) \circ m^{\cylinder}_{j},g_{1} \circ j \circ p(a_{0})} %
%
Moreover, the following diagram in $\cl{A}$ commutes.   

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=7 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| a_{1} \& |(1-0)| a^{\cylinder}_{j} \\ 
|(0-1)| a_{1} \& |(1-1)| \cyl(a_{1})   \\
};
	
\draw[->] (0-0) to node[auto] {$d^{1}_{j}$} (1-0);
\draw[->] (0-0) to node[auto,swap] {$id$} (0-1);
\draw[->] (1-0) to node[auto] {$m^{\cylinder}_{j}$} (1-1);
\draw[->] (1-1) to node[auto] {$p(a_{1})$} (0-1);
\draw[->] (0-0) to node[auto,swap] {$i_{0}(a_{1})$} (1-1);
\end{tikzpicture} 

\end{diagram} %
%
Thus the following diagram in $\cl{A}$ commutes. \tri{a_{1},a^{\cylinder}_{j},a_{3},d^{1}_{j},g_{1} \circ p(a_{1}) \circ m^{\cylinder}_{j},g_{1}} %
%
We now have that the following diagram in $\cl{A}$ commutes. \pushout[{4,3,3}]{a_{0},\cyl(a_{0}),a_{1},a^{\cylinder}_{j},a_{3},i_{0}(a_{0}),d^{0}_{j},j,d^{1}_{j},g_{1} \circ j \circ p(a_{0}), g_{1}, g_{1} \circ p(a_{1}) \circ m^{\cylinder}_{j}} %
%
Appealing to the universal property of $a^{\cylinder}_{j}$, it follows that the diagram \sq[{5,3}]{a^{\cylinder}_{j},a_{2},\cyl(a_{1}),a_{3},u,f,m^{\cylinder}_{j},g_{1} \circ p(a_{1})} in $\cl{A}$ commutes. % 
%
Since $f$ has the covering homotopy extension property with respect to $j$, we deduce there is an arrow \ar{\cyl(a_{1}),a_{2},l} of $\cl{A}$ such that the following diagram in $\cl{A}$ commutes. \liftingsquare[{5,3.5}]{a^{\cylinder}_{j},a_{2},\cyl(a_{1}),a_{3},u,f,m^{\cylinder}_{j},g_{1} \circ p(a_{1}),l} %
%
Let $x$ denote the arrow \ar[5]{a_{1},a_{2},l \circ i_{1}(a_{1})} of $\cl{A}$. We claim that the following diagram in $\cl{A}$ commutes. \liftingsquare{a_{0},a_{2},a_{1},a_{3},g_{0},f,j,g_{1},x} 

Firstly, note that the following diagram in $\cl{A}$ commutes. \liftingsquare[{4,4}]{\cyl(a_{0}),\cyl(a_{1}),a^{\cylinder}_{j},a_{2},\cyl(j),l,d^{0}_{j},u,m^{\cylinder}_{j}} Hence the following diagram in $\cl{A}$ commutes. \trapeziumsthree[{4,3.5,-1,0}]{a_{0},a_{1},\cyl(a_{0}),\cyl(a_{1}),a^{\cylinder}_{j},a_{2},j,x,i_{1}(a_{0}),d^{0}_{j},u,i_{1}(a_{1}),\cyl(j),l} %
%
Moreover, the following diagram in $\cl{A}$ commutes, by appeal to the definition of $u$ and $h$. 

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=3 em, row sep=3 em, nodes={anchor=center}]
{ 
|(0-0)| a_{0}  \&[1em]                     \& |(2-0)| \cyl(a_{0}) \\
|(0-1)| a_{2}  \& |(1-1)| \cyl(a_{2}) \& |(2-1)| a^{\cylinder}_{j} \\
               \&                     \& |(2-2)| a_{2} \\
};
	
\draw[->] (0-0) to node[auto] {$i_{1}(a_{0})$} (2-0);
\draw[->] (0-0) to node[auto,swap] {$g_{0}$} (0-1);
\draw[->] (0-1) to node[auto,swap] {$i_{1}(a_{2})$} (1-1);
\draw[->] (2-0) to node[auto,swap] {$\cyl(g_{0})$} (1-1);
\draw[->] (2-0) to node[auto] {$d^{0}_{j}$} (2-1);
\draw[->,bend right=45] (0-1) to node[auto,swap] {$id$} (2-2);
\draw[->] (1-1) to node[auto,swap] {$h$} (2-2);
\draw[->] (2-1) to node[auto] {$u$} (2-2);
\end{tikzpicture} 

\end{diagram} %
%
Putting the last two observations together, we have that the following diagram in $\cl{A}$ commutes. \tri{a_{0},a_{1},a_{2},j,x,g_{0}} 

Secondly, the following diagram in $\cl{A}$ commutes. \squareofthreetrianglestwo[{3,3.5,0,0}]{a_{1},a_{2},\cyl(a_{1}),a_{1},a_{3},x,f,id,g_{1},i_{1}(a_{1}),l,p(a_{1})} %
%
This completes the proof of the claim.

\end{proof}

\end{chapter}

