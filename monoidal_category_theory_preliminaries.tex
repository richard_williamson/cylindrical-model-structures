\begin{chapter}{Monoidal category theory preliminaries} \label{MonoidalCategoryTheoryPreliminariesChapter}

In \ref{StructuresUponAnIntervalChapter} we shall introduce the notion of an interval in a monoidal category $\cl{A}$. Under natural hypotheses, a structured interval in $\cl{A}$ will give rise to a structured cylinder and a structured co-cylinder in $\cl{A}$. 

We shall need the notion of an exponentiable object of $\cl{A}$. There are two possible definitions, as we shall not assume that our monoidal structures are symmetric. We make the following choice.

\begin{assum} Let $(\cl{A},\otimes)$ be a monoidal category. Let $1$ denote its unit object, and let $\lambda$ denote its natural isomorphism \ar{- \otimes 1, {-.}} \end{assum}

\begin{defn} An object $a$ of $\cl{A}$ is {\em exponentiable} with respect to $\otimes$ if the functor \ar[4]{\cl{A},\cl{A},- \otimes a} admits a right adjoint, which we shall denote by \ar{\cl{A},\cl{A}.,{(-)^{a}}} \end{defn}

\begin{rmk} \label{UnitExponentiableRemark} We have that $1$ is exponentiable with respect to $\otimes$, since the natural isomorphism \ar[7]{{\mathsf{Hom}_{\cl{A}}(- \otimes 1,-)},{\mathsf{Hom}_{\cl{A}}(-,-)},{\mathsf{Hom}_{\cl{A}}(\lambda^{-1},-)}} exhibits the identity functor as a right adjoint of $- \otimes 1$. \end{rmk} 

\begin{notn} Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$ such that both $a_{0}$ and $a_{1}$ are exponentiable with respect to $\otimes$. Then, for any object $a$ of $\cl{A}$, we have a natural isomorphism \ar[4]{{\mathsf{Hom}_{\cl{A}}(- \times a_{0}, a)},{\mathsf{Hom}_{\cl{A}}(-,a^{a_{0}})},\mathsf{adj}(a_{0})} and a natural isomorphism \ar[4]{{\mathsf{Hom}_{\cl{A}}(- \times a_{1}, a)},{\mathsf{Hom}_{\cl{A}}(-,a^{a_{1}}).},\mathsf{adj}(a_{1})} %
%
Let \ar{{a^{a_{1}}},{a^{a_{0}}},{a^{f}}} denote the arrow of $\cl{A}$ corresponding via the Yoneda lemma to the following natural transformation.  

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=5 em, row sep=4 em, nodes={anchor=center}]
{ 
|(0-0)| \mathsf{Hom}_{\cl{A}}(-,a^{a_{1}}) \& |(1-0)| \mathsf{Hom}_{\cl{A}}(- \otimes a_{1},a) \\ 
|(0-1)| \mathsf{Hom}_{\cl{A}}(-,a^{a_{0}}) \& |(1-1)| \mathsf{Hom}_{\cl{A}}(- \otimes a_{0}, a) \\
};
	
\draw[->] (0-0) to node[auto] {$\mathsf{adj}(a_{1})^{-1}$} (1-0);
\draw[->] (1-0) to node[auto] {$\mathsf{Hom}_{\cl{A}}(- \otimes f,a)$} (1-1);
\draw[->,dashed] (0-0) to (0-1);
\draw[<-] (0-1) to node[auto,swap] {$\mathsf{adj}(a_{0})$} (1-1);

\end{tikzpicture} 

\end{diagram} 

\end{notn} 

\begin{rmk} Let \ar{a_{0},a_{1},f} be an arrow of $\cl{A}$ such that both $a_{0}$ and $a_{1}$ are exponentiable with respect to $\otimes$. Associating to an object $a$ of $\cl{A}$ the arrow \ar{{a^{a_{1}}},{a^{a_{0}}},{a^{f}}} of $\cl{A}$ defines a natural transformation \ar{{(-)^{a_{1}}},{(-)^{a_{0}}.},{{(-)}^{f}}} \end{rmk}

\begin{defn} The monoidal structure upon $\cl{A}$ defined by $\otimes$ is {\em closed} if, for every object $a$ of $\cl{A}$, the functor \ar[4]{\cl{A},\cl{A},a \otimes -} admits a right adjoint. \end{defn}

\begin{rmk} Suppose that the monoidal structure upon $\cl{A}$ defined by $\otimes$ is symmetric. This monoidal structure is closed if and only if every object of $\cl{A}$ is exponentiable with respect to $\otimes$. \end{rmk}

\begin{rmk} Let \sq{a_{0},a_{1},a_{2},a_{3},f_{0},f_{1},f_{2},f_{3}} be a co-cartesian diagram in $\cl{A}$.  Let $a$ be an object of $\cl{A}$ such that the following diagram in $\cl{A}$ is co-cartesian. \sq[{4,3}]{a \otimes a_{0}, a \otimes a_{1}, a \otimes a_{2}, a \otimes a_{3}, a \otimes f_{0}, a \otimes f_{1}, a \otimes f_{2}, a \otimes f_{3}} %
%
If $a_{0}$, $a_{1}$, $a_{2}$, and $a_{3}$ are exponentiable with respect to $\otimes$, the following diagram in $\cl{A}$ is cartesian. \sq{{a^{a_{3}}},{a^{a_{2}}},{a^{a_{1}}},{a^{a_{0}}},{a^{f_{3}}},{a^{f_{2}}},{a^{f_{1}}},{a^{f_{0}}}} \end{rmk} 

\end{chapter}
